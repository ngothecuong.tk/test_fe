import { IItem } from '../../items/type'
import { IItemVariants } from '../../item_variants/type'

export interface ICartContextProps {
  cartItemVariants: {[key: string]: ICarts}
  setCartItemVariants: (cartItemVariants: {[key: string]: ICarts}) => void
  getCartItemVariants: (userID: string) => void
  loadingCart: boolean
  setLoadingCart: (loadingCart: boolean) => void
  removeCartItemVariant: (cartItemVariantID: string) => void
  didClick: boolean
  setDidClick: (isCheckClick: boolean) => void
  addToCart: (cart: IResponseDataAddToCart, item: IItem) => void
}
export interface ICarts {
  cartItemVariantID: string,
  userID: string,
  itemVariantID: IItemVariants
}
export interface IResponseDataAddToCart {
  cartItemVariantID: string,
  userID: string,
  itemVariantID: string
}