import { Box, Text, Button } from 'rebass'
import { Input } from '@rebass/forms'

export const CartPayment: React.FC = () => {

  return (
    <Box sx={{
          background: 'rgba(0,0,0, 0.2)',
          padding: '22px 22px 25px',
    }}>
      <Text
        sx={{
          fontSize: '14px',
          color: '#fff',
          textAlign: 'left',
          mb: '10px',
      }}>
        Have A Promo Code ?
      </Text>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
      }}>
        <Input
          sx={{
            flexGrow: '1',
            marginRight: '12px',
            border: '1px solid #ded1d1',
            outline: 'none',
            fontSize: '15px',
            '&::-webkit-input-placeholder': {
              color: '#cec3c3',
            },
            background: '#fff'
          }}
          placeholder='Promo Code'
        />
        <Button
          sx={{
            padding: '9px',
            minWidth: '85px',
            cursor: 'pointer',
            fontSize: '15px',
            background: 'linear-gradient(135deg,#75cbbe,#63ca7c)',
            color: '#fff'
          }}>
            Apply
        </Button>
      </Box>
      <Box
        sx={{
          borderTop: '1px solid #dedede',
          borderBottom: '1px solid #dedede',
          pb: '8px',
          mt: '20px',
          mb: '25px',
      }}>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'space-between',
            my: '15px',
          }}
        >
          <Text as='span' sx={{
            fontSize: '15px',
            color: '#fff',
            fontFamily: '"Motiva Sans", Sans-serif',
          }}>
            Subtotal:
          </Text>
          <Text sx={{
            fontSize: '15px',
            color: '#fff',
            fontFamily: '"Motiva Sans", Sans-serif',
          }}>
             123
          </Text>
        </Box>
        <Box
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
        }}>
          <Text
            as='span'
            sx={{
              color: '#fff',
              fontWeight: 'bold',
              fontFamily: '"Motiva Sans", Sans-serif',
          }}>
            Total:
          </Text>
          <Text
            sx={{
              fontSize: '20px',
              color: '#fff',
              fontFamily: '"Motiva Sans", Sans-serif',
          }}>
              456
          </Text>
        </Box>
      </Box>
      <Box>
        <Button
          sx={{
            p: '17px',
            width: '100%',
            cursor: 'pointer',
            fontWeight: '500',
            fontSize: '15px',
            borderRadius: '2px',
            textTransform: 'capitalize',
            background: 'linear-gradient(135deg,#75cbbe,#63ca7c)',
            color: '#fff'
        }}>
          Proceed to checkout
        </Button>
      </Box>
    </Box>
  )
}