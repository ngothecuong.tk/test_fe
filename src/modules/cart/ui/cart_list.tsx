import React from 'react'
import { Box } from 'rebass'
import { ICartContextProps } from '../type'
import useCartContext from '../logic/providers'
import { CartItem } from './cart_item'

export const CartList: React.FC = () => {
  const { cartItemVariants }: ICartContextProps = useCartContext()

  return (
    <Box sx={{
      bg: 'rgba(0,0,0, 0.2)',
      mb: ['12px', '20px'],
    }}>
      <Box sx={{
        width: '100%',
        paddingBottom: '1px',
        paddingTop: '1px'
      }}>
        {Object.values(cartItemVariants)?.map((cartItem) => {
          return (
            <CartItem
              key={cartItem.cartItemVariantID}
              cartItem={cartItem.itemVariantID}
              cartItemVariantID={cartItem.cartItemVariantID}
            />
          )
        })}
      </Box>
    </Box>
  )
}
