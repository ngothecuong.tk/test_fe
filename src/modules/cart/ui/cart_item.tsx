import React, { useCallback } from 'react'
import { Box, Text, Image } from 'rebass'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import { IItemVariants } from '../../item_variants/type'
import { ICartContextProps } from '../type'
import useCartContext from '../logic/providers'

interface IInitialProps {
  cartItem: IItemVariants
  cartItemVariantID: string
}

export const CartItem: React.FC<IInitialProps> = ({cartItem, cartItemVariantID}) => {

  const { removeCartItemVariant, didClick } : ICartContextProps = useCartContext()

  const handleRemoveCartItem = useCallback((cartItemID: string) => {
    if (didClick) {
      removeCartItemVariant(cartItemID)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [didClick])

  return (
    <Box variant='box__cart_item'>
      <Box sx={{
        minWidth: '25px',
        alignSelf: 'center',
      }}>
        <input
          type='checkbox'
          style={{
            width: '16px',
            height: '16px',
            margin: '0',
          }}
        />
      </Box>
      <Box sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'flex-start',
          flexGrow: '10000',
          flexBasis: 'auto',
          marginLeft: '5px'
        }}>
          <Box sx={{
            width: '100px',
            height: '100px'
          }}>
            <Image src={cartItem.thumbnail}
              sx={{
                objectFit: 'cover',
                width: '100%',
                height: '100%'
              }}
            />
          </Box>
          <Box sx={{marginLeft: '20px', textAlign: 'left'}}>
            <Text sx={{
              textTransform: 'capitalize',
              color: '#8dd294',
              mb: '10px',
              fontSize: '16px'
            }}>
              {cartItem.name}
            </Text>
            <Text sx={{
              textTransform: 'capitalize',
              color: '#fff',
              fontSize: '12px',
            }}>
              Sell By: {cartItem.userID.displayName}
            </Text>
          </Box>
        </Box>
      <Box sx={{
        position: 'relative',
        display: 'flex',
        flexGrow: '1',
        alignItems: 'flex-end',
        flexDirection: 'column',
        textAlign: 'left'
      }}>
        <Box sx={{textAlign: 'right', marginBottom: '10px'}}>
          <Text sx={{
            textDecoration: 'line-through',
            color: '#56707f',
            fontSize: '15px',
            mb: '5px',
          }}>
            {cartItem.price} {cartItem.currency}
          </Text>
          <Text sx={{
            color: '#fff',
            fontSize: '20px',
          }}>
            {cartItem.discountPrice} {cartItem.currency}
          </Text>
        </Box>
        <Box sx={{
          display: 'flex',
          alignItems: 'center',
          color: '#56707f',
          cursor: 'pointer',
          ':hover': {
            color: '#8dd294',
          }}}
          onClick={() => handleRemoveCartItem(cartItemVariantID)}
        >
          <FontAwesomeIcon
            icon={faTrashAlt}
            style={{
              width: '15px',
              height: '15px',
              marginRight: '5px'
            }}
          />
          <Text sx={{
              fontSize: '15px',
              textTransform: 'capitalize'
            }}>
            remove
          </Text>
        </Box>
      </Box>
    </Box>
  )
}