import { convertArrayObjectToObject } from '../../../helpers/convert_array_to_object'
import { ICarts, IResponseDataAddToCart } from '../type'
import { ultis } from 'react-cores'

export const getCartItemVariantsFromApi = async (param : {[key: string]: string}) => {
  try {
    const res = await ultis.api({path: 'cartItemVariants', method: 'GET', params: param})
    const formatData = convertArrayObjectToObject<ICarts>(res?.data?.list, 'cartItemVariantID')

    return {
      list: formatData
    }
  } catch (error) {
    throw error
  }
}

export const postCartItemVariantsToApi = async (param: {[key: string]: string}) => {
  try {
    const res = await ultis.api({path: 'cartItemVariants', method: 'POST', data: param})
    const data: IResponseDataAddToCart = res?.data

    return {
      list: data
    }
  } catch (error) {
    throw error
  }
}

export const removeCartItemVariantsFromApi = async (cartItemVariantID: string) => {
  try {
    const res = await ultis.api({path: `cartItemVariants/${cartItemVariantID}`, method: 'DELETE', data: {}})

    return res
  } catch (error) {
    throw error
  }
}