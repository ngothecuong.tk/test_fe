import React, { createContext, useCallback, useContext, useState } from 'react'
import { IResponseDataType } from '../../../types'
import { ICartContextProps, ICarts } from '../type'
import { removeCartItemVariantsFromApi, getCartItemVariantsFromApi } from './api'
import { hooks } from 'react-cores'

export const CartContext = createContext<ICartContextProps>(null!)

export const CartProvider: React.FC = ({ children }) => {
  const { enqueueSnackbar }: hooks.WithSnackbarProps = hooks.useNoti()
  const [cartItemVariants, setCartItemVariants] = useState<{[key: string]: ICarts}>({})
  const [loadingCart, setLoadingCart] = useState(false)
  const [didClick, setDidClick] = useState<boolean>(true)

  const getCartItemVariants = useCallback(
    (userID: string) => {
    setLoadingCart(true)
    const params = {
      userID: userID || ''
    }
    void getCartItemVariantsFromApi(params)
      .then((response: IResponseDataType<ICarts>) => {
        setCartItemVariants(response.list)
      })
      .finally(() => {
        setLoadingCart(false)
      })
  }, [])

  const addToCart = useCallback((data, item) => {
    const { cartItemVariantID, userID }: ICarts = data
    cartItemVariants[data.cartItemVariantID] = {
      cartItemVariantID,
      userID,
      itemVariantID: item
    }
    setCartItemVariants({...cartItemVariants})
    setDidClick(true)
  }, [cartItemVariants])

  const removeCartItemVariant = useCallback((cartItemVariantID: string) => {
    setDidClick(false)
    void removeCartItemVariantsFromApi(cartItemVariantID)
      .then((response) => {
        if (response?.status === 200) {
          enqueueSnackbar('The item has been removed from your cart', {variant: 'success'})
          handleRemoveCartItemVariant(cartItemVariantID)
        }
      })
      .catch(() => {
        setDidClick(true)
      })
      // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cartItemVariants])

  const handleRemoveCartItemVariant = useCallback((cartItemVariantID: string) => {
    delete cartItemVariants[cartItemVariantID]
    setCartItemVariants({...cartItemVariants})
    setDidClick(true)
  }, [cartItemVariants])

  return (
    <CartContext.Provider
      value={{
        cartItemVariants,
        setCartItemVariants,
        getCartItemVariants,
        loadingCart,
        setLoadingCart,
        removeCartItemVariant,
        didClick,
        setDidClick,
        addToCart
      }}
    >
      {children}
    </CartContext.Provider>
  )
}

export default function useCartContext() {
  const context = useContext(CartContext)
  if (!context) {
    throw new Error('useCartContext must be used within a CartProvider')
  }

  return context
}