import React, { useEffect, useMemo } from 'react'
import { Box, Text, Image } from 'rebass'
import { CartPayment } from './ui/cart_payment'
import { ICartContextProps } from './type'
import useCartContext from './logic/providers'
import { Link } from 'react-router-dom'
import { Loading } from '../../component/loading'
import { CartList } from './ui/cart_list'
import { IAuthUser } from '../auth/types'
import useAuthContext from '../auth/logic/provider'

export const Cart: React.FC = () => {
  const {cartItemVariants, getCartItemVariants, loadingCart}: ICartContextProps = useCartContext()
  const {userInfo}: IAuthUser = useAuthContext()

  useEffect(() => {
    if (userInfo.userID === '') return
    if (Object.values(cartItemVariants).length === 0) {
      getCartItemVariants(userInfo.userID)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userInfo])

  const numberItemInCart = useMemo(() => Object.values(cartItemVariants).length, [cartItemVariants])

  return (
    <Box sx={{width: '100%', overflow: 'hidden'}}>
      <Box sx={{width: ['auto', 'auto', 'auto', '990px'], p: '0 25px', m: '0 auto'}}>
        {loadingCart && <Loading/>}
        {!loadingCart && userInfo.userID !== '' && numberItemInCart === 0 &&
          <Box sx={{p: '3rem'}}>
            <Image src='https://quochuy-13.github.io/FashionStore/static/media/empty-cart.f9db2821.svg'
              sx={{width: '320px', mb: '10px'}}
            />
            <Text sx={{
              color: '#fff',
              fontSize: '18px',
              fontFamily: '"Motiva Sans", Sans-serif',
            }}>
              Your cart is empty.
              <Link to='/' style={{
                textDecoration: 'none',
                marginLeft: '5px',
                color: '#67c1f5'
              }}>
                Please fill it up
              </Link>
            </Text>
          </Box>
        }
        {!loadingCart && numberItemInCart !== 0 &&
          <Box sx={{display: ['block', 'block', 'block', 'flex']}}>
            <Box sx={{
              width: ['100%', '100%', '100%', '70%']
            }}>
              <CartList/>
            </Box>
            <Box sx={{
              ml: ['0px', '0px', '0px', '20px'],
              width: ['100%', '100%', '100%', '30%']
            }}>
              <CartPayment/>
            </Box>
          </Box>
        }
      </Box>
    </Box>
  )
}