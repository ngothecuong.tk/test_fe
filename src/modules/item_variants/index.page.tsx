import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import useItemsContext from '../items/logic/providers';
import { IItemsContextProps } from '../items/type';
import useItemVariantsContext from './logic/provider';
import { IItemVariantsContextProps, IItemVariants } from './type';
import { ItemInfo } from './ui/Item_info';
import { ListItemVariants } from './ui/list_item_variants';
import { IProjectsContextProps } from '../projects/type'
import useProjectsContext from '../projects/logic/providers';
interface IParamData {
  [key: string]: IItemVariants
}

type Params = { [K in keyof IParamData]: string }

export const ItemVariants: React.FC = () => {
  const param = useParams<Params>()
  const { getItemVariants }: IItemVariantsContextProps = useItemVariantsContext()
  const { getItemByItemID }: IItemsContextProps = useItemsContext()
  const { getProjects, projects}: IProjectsContextProps = useProjectsContext()
  useEffect(() => {
    window.scroll(0, 0)
    getItemVariants({itemID: param.itemID})
    getItemByItemID(param.itemID)
    if (Object.values(projects).length === 0) {
      getProjects({})
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [param])

  return (
    <>
      <ItemInfo/>
      <ListItemVariants/>
    </>
  );
}