import { convertArrayObjectToObject } from '../../../helpers/convert_array_to_object'
import { IItemVariants } from '../type'
import { ultis } from 'react-cores'

export const getItemVariantsFromApi = async (param: {[key: string]: string | number}) => {
  try {
    const res = await ultis.api({path: 'itemVariants', method: 'GET', params: param})
    const formatData = convertArrayObjectToObject<IItemVariants>(res?.data?.list, 'itemVariantID')

    return {
      list: formatData
    }
  } catch (error) {
    throw error
  }
}