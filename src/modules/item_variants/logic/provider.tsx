import React, { createContext, useCallback, useContext, useState } from 'react'
import { IResponseDataType } from '../../../types'
import { IItemVariantsContextProps, IItemVariants } from '../type'
import { getItemVariantsFromApi } from './api'

export const ItemVariantsContext = createContext<IItemVariantsContextProps>(null!)

export const ItemVariantsProvider: React.FC = ({ children }) => {
  const [itemVariants, setItemVariants] = useState({})
  const [loadingListItemVariants, setLoadingListItemVariants] = useState(false)

  const getItemVariants = useCallback(
    (query: {
      itemID?: string
      limit?: number
      offset?: number
      sortBy?: string
      sortDirection?: string
    }) => {
      setLoadingListItemVariants(true)
      const params = {
        ...(query.itemID && {
          itemID: query.itemID
        }),
        limit: query.limit || 10,
        offset: query.offset || 0,
        sortBy: query.sortBy || 'createdAt',
        sortDirection: query.sortDirection || 'ASC'
      }
      void getItemVariantsFromApi(params)
        .then((response: IResponseDataType<IItemVariants>) => {
          setItemVariants(response.list)
          setLoadingListItemVariants(false)
        })
        .catch(() => {
          setLoadingListItemVariants(false)
        })
  }, [])

  return (
    <ItemVariantsContext.Provider
      value={{
        itemVariants,
        setItemVariants,
        getItemVariants,
        loadingListItemVariants,
        setLoadingListItemVariants,
      }}
    >
      {children}
    </ItemVariantsContext.Provider>
  )
}

export default function useItemVariantsContext() {
  const context = useContext(ItemVariantsContext)
  if (!context) {
    throw new Error('useItemVariantsContext must be used within a ItemVariantsProvider')
  }

  return context
}