import { useCallback } from 'react';
import { Box, Flex } from 'rebass';
import { Loading } from '../../../component/loading';
import useItemVariantsContext from '../logic/provider';
import { IItemVariantsContextProps } from '../type';
import { ItemVariant } from '../ui/item_variant';

export const ListItemVariants: React.FC = () => {
  const {itemVariants, loadingListItemVariants }: IItemVariantsContextProps = useItemVariantsContext()

  const RenderItemVariants = useCallback(() => {
    if (!Object.keys(itemVariants)?.length) {
      return <Box>There is no item that you are looking for.</Box>
    }

    return (
      <Flex sx={{
        flexWrap: 'wrap',
        mx: '-5px'
      }}>
        {Object.values(itemVariants)?.map((each) => {
          return(
            <Box sx={{
              width: ['50%', '33.33333333%', '33.33333333%', '20%'],
              p: '0 5px',
              mb: '15px'
            }} key={each.itemVariantID}>
              <ItemVariant item={each}/>
            </Box>
          )})}
      </Flex>
    )
  }, [itemVariants])

  return (
    <>
      {loadingListItemVariants &&
          <Flex sx={{height: '250px',
            justifyContent: 'center',
            alignItems: 'center'
          }}>
            <Loading/>
          </Flex>
      }
      {!loadingListItemVariants && (
        <Box sx={{py: '20px'}}>
          <Box variant='box__home_container'>
            <RenderItemVariants/>
          </Box>
        </Box>
      )}
    </>
  );
}