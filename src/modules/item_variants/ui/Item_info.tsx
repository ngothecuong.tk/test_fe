import { useCallback, useMemo } from 'react';
import { Box, Flex, Image, Text } from 'rebass';
import { Loading } from '../../../component/loading';
import useItemsContext from '../../items/logic/providers';
import { IItemsContextProps } from '../../items/type';
import { IProjectsContextProps } from '../../projects/type'
import useProjectsContext from '../../projects/logic/providers';

export const ItemInfo: React.FC = () => {

  const {itemByItemID, loadingByItemID }: IItemsContextProps = useItemsContext()
  const {projects}: IProjectsContextProps = useProjectsContext()

  const projectFilter = useMemo(() => Object.values(projects).filter(project =>
    project.projectID === itemByItemID?.projectID), [projects, itemByItemID])

  const RenderInfoItem = useCallback(() => {
    return (
      <Box sx={{
        background: '#202023',
        py: '20px'
      }}>
        <Box variant='box__home_container'>
          <Flex>
            <Box>
              <Image
                src={itemByItemID?.thumbnail}
                sx={{
                  width: '250px',
                  height: '250px',
                  objectFit: 'cover'
                }}
              />
            </Box>
            <Box sx={{ml: '30px'}}>
              <Box sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                justifyContent: 'flex-start',
                textAlign: 'left'
              }}>
                <Text
                  sx={{
                    color: 'rgba(255,255,255,1)',
                    fontSize: '35px',
                    mb: '5px'
                }}>
                  {itemByItemID?.name}
                </Text>
                <Flex sx={{alignItems: 'center', mb: '15px'}}>
                  <Image sx={{
                    maxWidth: '22px',
                    maxHeight: '22px',
                    width: '100%',
                    height: '100%',
                  }} src={projectFilter[0]?.logo}/>
                  <Text
                    sx={{
                      color: 'rgba(255,255,255,1)',
                      fontSize: '20px',
                      ml: '5px'
                  }}>
                    {projectFilter[0]?.name}
                  </Text>
                </Flex>
                <Text
                  sx={{
                  fontSize: '16px',
                  color: 'rgba(255,255,255,1)',
                  fontFamily: '"Motiva Sans", Sans-serif',
                  mb: '15px'
                }}>
                  {itemByItemID?.description}
                </Text>
                <Text sx={{
                  color: '#63ca7c',
                  marginBottom: '5px'
                }}>
                  HighestPrice : {itemByItemID?.highestPrice}
                </Text>
                <Text sx={{
                  color: 'rgba(250,203,83,1)'
                }}>
                  LowestPrice : {itemByItemID?.lowestPrice}
                </Text>
              </Box>
            </Box>
          </Flex>
        </Box>
      </Box>
    )
  }, [itemByItemID, projectFilter])

  return (
    <>
      {loadingByItemID && <Loading/>}
      {!loadingByItemID && <RenderInfoItem/>}
    </>
  );
}