import { IUserInfo } from '../../auth/types'

export interface IItemVariantsContextProps {
  itemVariants:  {[key: string]: IItemVariants}
  setItemVariants: (itemVariants: {[key: string]: IItemVariants}) => void
  getItemVariants: (query: {[key: string]: string | number}) => void
  loadingListItemVariants: boolean
  setLoadingListItemVariants: (loadingListItemVariants: boolean) => void
}

export interface IItemVariants {
  itemVariantID: string,
  userID: IUserInfo,
  itemID: string,
  companyID: string,
  projectID: string,
  categoryID: string,
  name: string,
  description: string,
  photos: string[],
  thumbnail: string,
  price: number,
  discountPrice: number,
  highestPrice?: number,
  lowestPrice?: number,
  tags?: string[],
  createdAt?: number,
  updatedAt?: number,
  currency: string
}