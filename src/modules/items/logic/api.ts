import { convertArrayObjectToObject } from '../../../helpers/convert_array_to_object'
import { IItem } from '../type'
import { ultis } from 'react-cores'

export const getItemsFromApi = async (param: {[key: string]: string | number}) => {
  try {
    const res = await ultis.api({path: 'items', method: 'GET', params: param })
    const formatData = convertArrayObjectToObject<IItem>(res?.data?.list, 'itemID')

    return {
      list: formatData
    }
  } catch (error) {
    throw error
  }
}

export const getItemsByEndpointFromApi = async (endpoint: string, param?: {[key: string]: string | number}) => {
  try {
    const res = await ultis.api({path: `items/${endpoint}`, method: 'GET', params : param})
    const formatData = convertArrayObjectToObject<IItem>(res?.data?.list, 'itemID')

    return {
      list: formatData
    }
  } catch (error) {
    throw error
  }
}

export const getItemByItemIDFromApi = async (itemID: string) => {
  try {
    const res = await ultis.api({ path: `items/${itemID}`, method: 'GET'})

    return {
      list: res?.data
    }
  } catch (error) {
    throw error
  }
}

export const getSearchItemFromApi = async (param: {[key: string]: string | number}) => {
  try {
    const res = await ultis.api({path: 'items/search', method: 'GET', params: param})
    const formatData = convertArrayObjectToObject<IItem>(res?.data?.list, 'itemID')

    return {
      list: formatData
    }
  } catch (error) {
    throw error
  }
}