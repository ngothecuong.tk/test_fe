import React, { createContext, useCallback, useContext, useState } from 'react'
import { IResponseDataType } from '../../../types'
import { IItem, IItemsContextProps } from '../type'
import { getItemByItemIDFromApi, getItemsByEndpointFromApi, getItemsFromApi, getSearchItemFromApi } from './api'

export const ItemsContext = createContext<IItemsContextProps>(null!)

export const ItemsProvider: React.FC = ({ children }) => {
  const [items, setItems] = useState({})
  const [itemsByEndpoint, setItemsByEndpoint] = useState({})
  const [itemByItemID, setItemByItemID] = useState<IItem>(null!)
  const [loadingByItemID, setLoadingByItemID] = useState(false)
  const [loadingByEndpoint, setLoadingByEndpoint] = useState(false)
  const [searchItemResult, setSearchItemResult] = useState({})

  const getItems = useCallback(
    (query: {
      limit?: number
      offset?: number
    }) => {
    setLoadingByItemID(true)
    const params = {
      limit: query.limit || 10,
      offset: query.offset || 0
    }
    void getItemsFromApi(params)
    .then((response: IResponseDataType<IItem>) => {
      setItems(response.list)
    })
    .finally(() => {
      setLoadingByItemID(false)
    })
  }, [])

  const getItemsByEndpoint = useCallback(
    (endpoint: string, query: {
      categoryID?: string
    }) => {
    setLoadingByEndpoint(true)
    const params = {
      ...(query.categoryID && {
        categoryID: query.categoryID
      })
    }
    void getItemsByEndpointFromApi(endpoint, params)
      .then((response: IResponseDataType<IItem>) => {
        setItemsByEndpoint(response.list)
      })
      .finally(() => {
        setLoadingByEndpoint(false)
      })
  }, [])

  const getItemByItemID = useCallback((itemID: string) => {
    setLoadingByItemID(true)
    void getItemByItemIDFromApi(itemID)
      .then((response) => {
        setItemByItemID(response.list as IItem)
      })
      .finally(() => {
        setLoadingByItemID(false)
      })
  }, [])

  const getSearchItem = useCallback(
    (query: {
      name: string
      limit?: number
      offset?: number
    }) => {
      const params = {
        name: query.name,
        limit: query.limit || 5,
        offset: query.offset || 0
      }
      void getSearchItemFromApi(params)
        .then((response: IResponseDataType<IItem>) => {
          setSearchItemResult(response.list)
        })
  }, [])

  return (
    <ItemsContext.Provider
      value={{
        items,
        setItems,
        itemsByEndpoint,
        setItemsByEndpoint,
        itemByItemID,
        setItemByItemID,
        getItems,
        getItemsByEndpoint,
        getItemByItemID,
        loadingByItemID,
        setLoadingByItemID,
        loadingByEndpoint,
        setLoadingByEndpoint,
        searchItemResult,
        setSearchItemResult,
        getSearchItem
      }}
    >
      {children}
    </ItemsContext.Provider>
  )
}

export default function useItemsContext() {
  const context = useContext(ItemsContext)
  if (!context) {
    throw new Error('useItemsContext must be used within a ItemsProvider')
  }

  return context
}