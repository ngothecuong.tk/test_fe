import { Box, Flex, Image, Text } from 'rebass'
import useProjectsContext from '../../projects/logic/providers';
import { IProjectsContextProps } from '../../projects/type'
import {calculateDiscountPercentage} from '../../../helpers/calculate_discount_percentage'
import { useMemo } from 'react';
import { Link } from 'react-router-dom';
import {IItem} from '../type'

interface IInitialProps {
  item: IItem
}

export const Item: React.FC<IInitialProps> = ({item}) => {

  const {projects}: IProjectsContextProps = useProjectsContext()
  const projectFilter = useMemo(() => Object.values(projects).filter(project =>
    project.projectID === item.projectID), [projects, item.projectID])

  return (
    <Box variant='box__cardItem'>
      <Link to={`/items/${item.itemID}`} style={{display: 'flex',
        flexDirection: 'column', height: '100%', textDecoration: 'none'
      }}>
        <Flex sx={{
          justifyContent: 'center',
          alignItems: 'center',
          flex: '1'
        }}>
          <Image
            src={item.thumbnail}
            sx={{
              transitionDuration: '0.4s',
              transitionTimingFunction: 'cubic-bezier(0, 0.73, 0.48, 1)',
              borderRadius: '5px',
              objectFit: 'cover',
              width: ['150px', '130px', '145px', '128px'],
              height: ['150px', '130px', '145px', '128px']
            }}
          />
        </Flex>
        <Box variant='box__cardItem_content'>
          <Box sx={{
            mr: '8px',
            paddingBottom: '10px',
            flex: '1 1 0%',
          }}>
            <Text variant='text__cardItem_title' >
              {item.name}
            </Text>
            <Flex sx={{alignItems: 'center'}}>
              <Image sx={{
                maxWidth: '16px',
                width: '100%',
                height: '16px',
                p: '2px 6px 2px 0px',
                boxSizing: 'content-box'
              }} src={projectFilter[0]?.logo}/>
              <Text variant='text__cardItem_titleProject' >
                {projectFilter[0]?.name}
              </Text>
            </Flex>
          </Box>
          <Box variant='price' sx={{
                display: 'flex',
                justifyContent: 'end',
                fontSize: '14px',
                mr: '8px'
          }}>
            <Box variant='box__discount_percent'>
              {calculateDiscountPercentage(item.price, item.discountPrice)}
            </Box>
            <Flex sx={{
              backgroundColor: 'rgba(20,31,44,0.4)',
            }}>
              <Box variant='box__price_original'>
                {item.price}đ
              </Box>
              <Box variant='box__price_final'>
                {item.discountPrice}đ
              </Box>
            </Flex>
          </Box>
        </Box>
      </Link>
    </Box>
  );
}