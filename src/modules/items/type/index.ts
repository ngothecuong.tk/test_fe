export interface IItemsContextProps {
  items: {[key: string] : IItem}
  setItems: (items: {[key: string]: IItem}) => void
  itemsByEndpoint: {[key: string] : IItem}
  setItemsByEndpoint: (items: {[key: string]: IItem}) => void
  itemByItemID: IItem
  setItemByItemID: (itemByItemID: IItem) => void
  getItems: (query: {[key: string]: string}) => void
  getItemsByEndpoint: (endpoint: string, query: {[key: string]: string}) => void
  getItemByItemID: (itemID: string) => void
  loadingByItemID: boolean
  setLoadingByItemID: (loadingByItemID: boolean) => void
  loadingByEndpoint: boolean
  setLoadingByEndpoint: (loadingByEndpoint: boolean) => void
  searchItemResult: {[key: string] : IItem}
  setSearchItemResult: (items: {[key: string]: IItem}) => void
  getSearchItem: (query: {name: string, limit?: number, offset?: number}) => void
}

export interface IItem {
  itemID: string,
  companyID: string,
  projectID: string,
  categoryID: string,
  name: string,
  description: string,
  photos: string[],
  thumbnail: string,
  price: number,
  discountPrice: number,
  currency: string
  highestPrice?: number,
  lowestPrice?: number,
  tags?: string[],
  createdAt?: number,
  updatedAt?: number
}
export interface IParamItem {
  limit?: string
  offset?: string
  categoryID?: string
}