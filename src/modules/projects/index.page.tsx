import { useCallback, useEffect } from 'react'
import { useLocation } from 'react-router-dom'

import { Box, Flex } from 'rebass'
import { PageContent } from '../../component/Content/pagecontent'
import { Loading } from '../../component/loading'
import { convertStringToObjectUrl } from '../../helpers/convert_between_object_string'

import useProjectsContext from './logic/providers'
import { IProjectsContextProps } from './type'
import { ItemProject } from './ui/index'
const Buttons = [
  {
    title: 'ALL Product',
    link: '/newreleases',
    variant: 'link_common_button',
    description_Item: '',
  },
]

export const Project: React.FC = () => {
  // tslint:disable-next-line: no-shadowed-variable
  const { projects, getProjects, loadingByProject }: IProjectsContextProps = useProjectsContext()
  const location = useLocation()

  useEffect(() => {
    const queryObject = convertStringToObjectUrl(location.search)
    if (queryObject?.name) return
    getProjects({})
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const RenderItemProject = useCallback(() => {
    if (Object.keys(projects).length === 0) {
      return <Box>There is no game that you are looking for.</Box>
    }

    return (
      <Flex
        sx={{
          justifyContent: 'center',
          flexWrap: 'wrap',
          mx: '-5px',
          paddingTop: '10px',
          maxWidth: '960px',
          margin: 'auto',
        }}
      >
        {Object.values(projects)?.map((item, idx) => {
          return (
            <Box
              sx={{
                width: ['50%', '33.33333333%', '25%', '20%'],
                p: '0 5px',
                mb: '15px',
              }}
              key={idx}
            >
              <ItemProject project={item} />
            </Box>
          )
        })}
      </Flex>
    )
  }, [projects])

  return (
    <>
      {Buttons.map((item, index) => (
        <PageContent key={index} title={item.title} description_Item={item.description_Item} />
      ))}
      {loadingByProject ? (
        <Flex sx={{ height: '100px', justifyContent: 'center', alignItems: 'center' }}>
          <Loading />
        </Flex>
      ) : (
        <RenderItemProject />
      )}
    </>
  )
}
