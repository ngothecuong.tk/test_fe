import { convertArrayObjectToObject } from '../../../helpers/convert_array_to_object'
import { IParamProject, IProjects } from '../type'
import { ultis } from 'react-cores'

export const getProjectsFromApi = async ({
  address,
  categoryID,
  companyID,
  name,
  projectID,
  status,
  sortBy,
  sortDirection,
  limit,
  offset,
}: IParamProject) => {
  try {
    const res = await ultis.api({
      path: 'projects',
      method: 'GET',
      params: {
        address,
        categoryID,
        companyID,
        name,
        projectID,
        status,
        sortBy,
        sortDirection,
        limit,
        offset,
      },
      // tslint:disable-next-line:no-console
      errorHandler: (error) => console.log(error),
    })
    const formatData = convertArrayObjectToObject<IProjects>(res?.data?.list, 'projectID')

    return {
      list: formatData,
    }
  } catch (error) {
    throw error
  }
}
