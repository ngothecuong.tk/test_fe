import React, { createContext, useCallback, useContext, useState } from 'react'
import { IResponseDataType } from '../../../types'
import { IParamProject, IProjects, IProjectsContextProps } from '../type'
import { getProjectsFromApi } from './api'

export const ProjectsContext = createContext<IProjectsContextProps>(null!)

export const ProjectsProvider: React.FC = ({ children }) => {
  const [sortByParams, setSortByParams] = useState('')
  const [projects, setProjects] = useState({})
  const [loadingByProject, setLoadingByProject] = useState(false)

  const getProjects = useCallback(
    (query: IParamProject) => {
      setLoadingByProject(true)
      const params = !query.name
        ? query
        : {
            ...query,
            sortBy: query.sortBy || null,
            name: !query.name ? '' : query.name,
            limit: query.limit || 10,
            offset: query.offset || 0,
          }
      void getProjectsFromApi(params)
        .then((response: IResponseDataType<IProjects>) => {
          setProjects(response.list)
        })
        .finally(() => {
          setLoadingByProject(false)
        })
    },
    [sortByParams]
  )

  return (
    <ProjectsContext.Provider
      value={{
        projects,
        setProjects,
        getProjects,
        loadingByProject,
        setLoadingByProject,
        sortByParams,
        setSortByParams,
      }}
    >
      {children}
    </ProjectsContext.Provider>
  )
}

export default function useProjectsContext() {
  const context = useContext(ProjectsContext)
  if (!context) {
    throw new Error('useProjectsContext must be used within a ProjectsProvider')
  }

  return context
}
