import { Box, Flex, Image, Link, Text } from 'rebass'
import { IProjects } from '../type'

interface IInitialProps {
  project: IProjects
}

export const ItemProject: React.FC<IInitialProps> = ({project}) => {
  return (
    <Link sx= {{textDecoration: 'none'}} href={`/project/${project.projectID}`}>
    <Box variant='box__cardItem'>
      <Flex
        sx={{
          justifyContent: 'center',
          alignItems: 'center',
          flex: '1',
        }}
      >
        <Image
          src={!project?.photos?.length ? '' : project?.photos?.[0]}
          variant='image_item_project'
        />
      </Flex>
      <Box variant='box__cardItem_content' sx={{height: 'calc(100%-128px)'}}>
        <Box
          sx={{
            mr: '8px',
            paddingBottom: '10px',
            flex: '1 1 0%',
          }}
        >
          <Flex sx={{ alignItems: 'center' }}>
            <Image
            variant='image_item_project_child'
              src={project.logo}
            />
            <Text variant='text__cardItem_titleProject'>{project.name}</Text>
          </Flex>
          <Flex
            sx={{
              mt: '3px',
              color: '#C3CBDA',
              textTransform: 'capitalize',
              fontSize: '13px',
            }}
          >
            <Text
              sx={{
                fontWeight: '500',
              }}
            >
            Type:
            </Text>
            <Text
              sx={{
                fontWeight: '400',
                ml: '3px',
              }}
            >
              {project.type}
            </Text>
          </Flex>
        </Box>
        <Box
          variant='box__price_final'
          sx={{
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            fontFamily: '"Motiva Sans",Sans-serif',
            fontSize: '14px',
            paddingLeft: '0'
          }}
        >
          {project.description}
        </Box>
      </Box>
    </Box>
    </Link>
  )
}
