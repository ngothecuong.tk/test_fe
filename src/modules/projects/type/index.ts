export interface IProjectsContextProps {
  projects: { [key: string]: IProjects }
  setProjects: (groups: { [key: string]: IProjects }) => void
  getProjects: (query: IParamProject) => void
  sortByParams?: string
  setSortByParams: (sortBy: string) => void
  loadingByProject: boolean
  setLoadingByProject: (loadingByProject: boolean) => void
}

export interface IProjects {
  projectID: string
  categoryID: string
  companyID: string
  name: string
  email?: string
  phoneNumber?: string
  type?: string
  tag?: string[]
  logo: string
  photos?: string[]
  banner: string
  introVideo?: string
  intro?: string
  description: string
  status?: string
  createdAt?: number
  updatedAt?: number
  limit?: number
  offset?: number
}

export interface IParamProject {
  categoryID?: string
  projectID?: string
  companyID?: string
  name?: string
  address?: string
  status?: string
  sortBy?: string | null
  sortDirection?: string
  limit?: number
  offset?: number
}
