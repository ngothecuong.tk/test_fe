import { useEffect, useMemo } from 'react'
import { useParams } from 'react-router'
import { Box, Flex } from 'rebass'
import useProjectsContext from '../projects/logic/providers'
import { IProjects, IProjectsContextProps } from '../projects/type/index'
import { Gamedetail } from './ui/gamedetail'
interface IParamData {
  [key: string]: IProjects
}

type Params = { [K in keyof IParamData]: string }

export const Gamedetails: React.FC = () => {
  const param = useParams<Params>()
  const { projects, getProjects }: IProjectsContextProps = useProjectsContext()
  const projectFilter = useMemo(
    () => Object.values(projects)?.filter((project) => project.projectID === param.projectID),
    [projects, param.projectID]
  )

  useEffect(() => {
    getProjects({})
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <>
      <Flex
        sx={{
          flexWrap: 'wrap',
          mx: '-5px',
        }}
      >
        {projectFilter.map((each) => {
          return (
            <Box
              sx={{
                width: ['100%'],
                p: '0 5px',
                mb: '15px',
              }}
              key={each.projectID}
            >
              <Gamedetail project={each} />
            </Box>
          )
        })}
      </Flex>
    </>
  )
}
