import { Box } from '@material-ui/core'
import { useEffect, useMemo } from 'react'
import { useParams } from 'react-router-dom'
import { Breadcrumbs } from '../../../component/breadcrumb'
import useCategoriesContext from '../../categories/logic/provider'
import { ICategory, ICategoryContext } from '../../categories/types'

import { IProjects } from '../../projects/type/index'

interface IInitialProps {
  project: IProjects
}
interface IParamData {
  [key: string]: ICategory
}
type Params = { [K in keyof IParamData]: string }
export const Gamedetail: React.FC<IInitialProps> = ({ project }) => {
  const param = useParams<Params>()
  const { categories, getCategoriesAction }: ICategoryContext = useCategoriesContext()
  const categoriFind = useMemo(
    () => Object.values(categories).find((category) => category.categoryID === project.categoryID),
    [categories, project.categoryID]
  )

  const breadcrumbData = [
    {
      name: 'Home',
      link: '/',
    },
    {
      name: `${categoriFind?.name}`,
    },
    {
      name: `${project.name}`,
    },
  ]
  useEffect(() => {
    if (param?.categoryID === '') return
    getCategoriesAction(param?.categoryID)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [param])

    return (
      <>
        <Breadcrumbs breadCrumbData={breadcrumbData} />
        <Box
          sx={{
            maxWidth: '940px',
            margin: '0 auto',
            fontSize: '14px',
            color: '#b8b6b4',
          }}
        >
          {project?.description}
        </Box>
      </>
    )
}
