import { Flex } from 'rebass'
import { CommonButton } from '../../component/common_button'
import { FeaturedRecommend } from './ui/featured_recommend'
import { SpecialOffer } from './ui/special_offers'
import {PopularGame} from './ui/popular_game'
import {TabLists} from './ui/tab_list'
import { BannerHome } from './ui/banner'
interface IProps {
  children?: React.ReactNode
}
const Buttons = [
  {
    title: 'NEW RELEASES',
    link: '/trending',
    variant: 'link_common_button',
  },
  {
    title: 'SPECIALS',
    link: '/specials',
    variant: 'link_common_button',
  },
  {
    title: 'POPULAR UPCOMING',
    link: '/upcoming',
    variant: 'link_common_button',
  },
  {
    title: 'TOP SELLER',
    link: '/topsellers',
    variant: 'link_common_button',
  },
]

export const Home: React.FC<IProps> = () => {
  return (
    <>
      <BannerHome/>
      <Flex
        sx={{
          maxWidth: '960px',
          margin: '0 auto',
          justifyContent: 'center',
          flexWrap: 'wrap',
        }}
      >
        {Buttons.map((item, index) => (
          <CommonButton key={index} link={item.link} title={item.title} variant={item.variant} />
        ))}
      </Flex>
      <FeaturedRecommend />
      <SpecialOffer />
      <PopularGame />
      <TabLists />
    </>
  )
}
