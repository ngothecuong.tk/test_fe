import {Box, Text } from 'rebass'
import { IProjectsContextProps } from '../../projects/type';
import useProjectsContext from '../../projects/logic/providers';
import { Sliders } from '../../../component/slider';
import { SliderHomeSpecialOffer } from '../../../component/slider/slider_home_special_offers';
import { useMemo } from 'react';

export const SpecialOffer: React.FC = () => {

  const {projects}: IProjectsContextProps = useProjectsContext()
  const dataProjects = useMemo(() => Object.values(projects).splice(0, 6), [projects])

  const renderSlides = () =>
    dataProjects.map((value, index) => (
      <Box key={index} sx={{px: ['5px']}}>
        <SliderHomeSpecialOffer
          project={value}
        />
      </Box>
  ));

  return (
    <Box sx={{mt: '50px'}}>
      <Box variant='box__home_container'>
        <Text variant='text__title_slider'>
          special offers
        </Text>
        <Box sx={{m: '0 -5px'}}>
          <Sliders
            slideShow = {3}
            slideScroll = {3}
            showFade = {false}
            showNextArrow= {'-40px'}
            showPrevArrow= {'-40px'}
            showResponsive = {
              [
                {
                  breakpoint: 1024,
                  settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1,
                  }
                },
                {
                  breakpoint: 768,
                  settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1,
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  }
                },
              ]
            }
          >
            {renderSlides()}
          </Sliders>
        </Box>
      </Box>
    </Box>
  );
}