import { Box, Image, Text } from 'rebass'
import { Link } from 'react-router-dom';
interface IProps {
    title: string,
    sxBox: {},
    sxText: {},
    sxImg: {},
    image: string,
}
export const GameBannerRight: React.FC<IProps> = ({ title, sxBox, sxText, sxImg, image }) => {
    return (
        <Box variant='box__games_right'>
            <Link to='/#'>
                <Box variant='box__gamesLink'
                    sx={{
                        ...sxBox
                    }}
                >
                    <Image sx={{
                        ...sxImg
                    }} src={image} />
                    <Text variant='text_title_game'
                        sx={{
                            ...sxText
                        }}
                    >
                        {title}
                    </Text>
                </Box>
            </Link>
        </Box>
    )
}