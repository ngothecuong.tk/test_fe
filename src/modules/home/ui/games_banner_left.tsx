import { Box, Image, Text } from 'rebass'
import { Link } from 'react-router-dom';
interface IProps {
    title: string,
    sxBox: {},
    sxText: {},
    sxImg: {},
    image: string,
}
export const GameBannerLeft: React.FC<IProps> = ({ title, sxBox, sxText, sxImg, image }) => {

    return (
        <Box variant='box__games_left'>
            <Link to='/#'>
                <Box variant='box__gamesLink'
                    sx={{
                        ...sxBox
                    }}
                >
                    <Text variant='text_title_game'
                        sx={{
                            ...sxText
                        }}
                    >
                        {title}
                    </Text>
                    <Image sx={{
                        ...sxImg
                    }} src={image} />
                </Box>
            </Link>
        </Box>
    )
}