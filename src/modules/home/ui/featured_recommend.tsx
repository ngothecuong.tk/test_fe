import { Box, Text } from 'rebass'
import { useEffect, useMemo } from 'react'
import { IProjectsContextProps } from '../../projects/type'
import useProjectsContext from '../../projects/logic/providers'
import { Sliders } from '../../../component/slider'
import { SliderHomeFeatured } from '../../../component/slider/slider_home_featured'
import { useParams } from 'react-router-dom'
import { ICategory } from '../../categories/types'
interface IParamData {
  [key: string]: ICategory
}

type Params = { [K in keyof IParamData]: string }
export const FeaturedRecommend: React.FC = () => {
  const { projects, getProjects }: IProjectsContextProps = useProjectsContext()
  const dataProjects = useMemo(() => Object.values(projects).splice(0, 6), [projects])
  const param = useParams<Params>()

  useEffect(() => {
    getProjects({ categoryID: param?.categoryID })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [param?.categoryID])

  const renderSlides = () =>
    dataProjects?.map((value, index) => <SliderHomeFeatured key={index} project={value} />)

  return (
    <Box variant='box__home_container'>
      <Text variant='text__title_slider'>
        featured & recommended
      </Text>
      <Box sx={{
        boxShadow: '0 0 7px 0px #000',
        maxWidth: '100%',
        margin: '5px auto 0 auto',
      }}>
        <Sliders
          slideShow = {1}
          slideScroll = {1}
          showFade = {false}
          showNextArrow= {'-44px'}
          showPrevArrow= {'-44px'}
        >
          {renderSlides()}
        </Sliders>
      </Box>
    </Box>
  );
}
