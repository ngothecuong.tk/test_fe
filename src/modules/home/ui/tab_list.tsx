import { useState, useEffect, useMemo, useCallback } from 'react'
import { Box, Flex } from 'rebass'
import { Item } from '../../items/ui/item'
import {  IItemsContextProps } from '../../items/type'
import useItemsContext from '../../items/logic/providers';
import { ICategory } from '../../categories/types'
import { useParams } from 'react-router-dom';
import { Loading } from '../../../component/loading';

const HomeTabLists = [
  {
    id: 1,
    title: 'Top sellers',
    endpoint: 'topSellerItems'
  },
  {
    id: 2,
    title: 'New and trending',
    endpoint: 'newAndTrendingItems'
  },
  {
    id: 3,
    title: 'Current Specials',
    endpoint: 'specialCurrentItems'
  },
  {
    id: 4,
    title: 'Upcoming releases',
    endpoint: 'upcomingReleaseItems'
  }
]
interface IParamData {
  [key: string]: ICategory
}

type Params = { [K in keyof IParamData]: string }
export const TabLists: React.FC = () => {
  const [activeTab, setActiveTab] = useState(1);
  const {itemsByEndpoint, getItemsByEndpoint, loadingByEndpoint}: IItemsContextProps = useItemsContext()
  const dataItems = useMemo(() => Object.values(itemsByEndpoint).splice(0, 20), [itemsByEndpoint]);
  const param = useParams<Params>()

  useEffect(() => {
    getItemsByEndpoint('topSellerItems', {categoryID: param?.categoryID})
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [param?.categoryID])

  const RenderItems = useCallback(() => {
    if (!Object.keys(dataItems)?.length) {
      return <Box>There is no item that you are looking for.</Box>
    }

    return (
      <Flex sx={{
        flexWrap: 'wrap',
        mx: '-5px'
      }}>
        {dataItems.map((each) => {
          return(
            <Box sx={{
              width: ['50%', '33.33333333%', '33.33333333%', '20%'],
              p: '0 5px',
              mb: '15px'
            }} key={each.itemID}>
              <Item item={each} />
            </Box>
          )})}
      </Flex>
    )
  }, [dataItems])

  return (
    <Box sx={{
      overflow: 'visible',
      pt: '1px',
      mt: '100px',
      background: 'linear-gradient( to bottom, rgba(42,71,94,1.0) 5%, rgba(42,71,94,0.0) 70%)'
    }}>
      <Box variant='box__home_container' sx={{mt: '-31px'}}>
        <Box variant='box__tab_lists'>
          {HomeTabLists.map((tab, index) => {
            return(
              <Box key={index}
                variant='box__tab_list_item'
                onClick={() => {
                  setActiveTab(tab.id)
                  getItemsByEndpoint(tab.endpoint, {categoryID: param?.categoryID})
                }}
                sx={{
                  color: activeTab === tab.id ? '#fff' : '4f94bc',
                  background: activeTab === tab.id ? '#2a475e' : 'transparent',
                  fontSize: activeTab === tab.id ? '14px' : '13px'
                }}
              >
                {tab.title}
              </Box>
            )
          })}
        </Box>
        <Box variant='box__tab_content'>
          {loadingByEndpoint && <Loading/>}
          {!loadingByEndpoint && <RenderItems/>}
        </Box>
      </Box>
    </Box>
  )
}