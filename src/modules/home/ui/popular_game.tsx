import {Box, Text } from 'rebass'
import { useEffect } from 'react';
import {  IItemsContextProps } from '../../items/type'
import useItemsContext from '../../items/logic/providers';
import { Sliders } from '../../../component/slider';
import { Item } from '../../items/ui/item';

export const PopularGame: React.FC = () => {

  const {items, getItems}: IItemsContextProps = useItemsContext()
  const dataItems = Object.values(items).splice(0, 10)
  useEffect(() => {
    getItems({})
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const renderSlides = () =>
    dataItems.map((value, index) => (
      <Box key={index} sx={{px: '5px'}}>
        <Item
        item={value}
      />
      </Box>
  ));

  return (
    <Box sx={{mt: '50px'}}>
      <Box variant='box__home_container'>
        <Text variant='text__title_slider'>
          POPULAR VR GAMES
        </Text>
        <Box sx={{margin: '0 -5px'}}>
          <Sliders
              slideShow = {5}
              slideScroll = {5}
              showFade = {false}
              showNextArrow= {'-36px'}
              showPrevArrow= {'-36px'}
              showResponsive = {
                [
                  {
                    breakpoint: 1024,
                    settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2,
                    }
                  },
                  {
                    breakpoint: 768,
                    settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    }
                  },
                  {
                    breakpoint: 479,
                    settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    }
                  },
                ]
              }
            >
              {renderSlides()}
          </Sliders>
        </Box>
    </Box>
    </Box>
  );
}