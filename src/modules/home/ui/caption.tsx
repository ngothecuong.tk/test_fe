import { Box, Image, Text } from 'rebass';

export const Caption = () => {

    return (
            <Box
                sx={{
                    marginTop: ['0px', '20px', '130px', '100px'],
                    textAlign: 'center',
                }}>
                <Box
                    sx={{
                        paddingTop: ['0px', '0px', '130px', '180px']
                    }}
                >
                    <Image maxWidth={['100px', '100px', '200px']} src='https://cdn-front-static.dmarket.com/prod/v1-183-14/assets/img/home/dmarket-logo.svg' />
                </Box>
                <Box mt='25px'>
                    <Text variant='text_caption__title' as='h1' letterSpacing={['8px', 'none']}>
                        Nft metaverse
                    </Text>
                </Box>
                <Text sx={{
                    marginTop: '20px',
                    fontSize: '16px',
                    textTransform: 'uppercase',
                    fontWeight: '900',
                }}>
                    Since 2017
                </Text>
            </Box>
    )
}