import { Box, Flex, Text } from 'rebass'
import { SvgPaymentsIcon } from './svg_paymentIcon';
import { GameBannerRight } from './games_banner_right';
import { GameBannerLeft } from './games_banner_left';
import { Caption } from './caption';
import { CommonButton } from '../../../component/common_button';

const DataButton = [
  {
    title: 'Get NFT drops!',
    variant: 'link_button_banner_home_page_left',
    link: '#/',
  },
  {
    title: 'Trade now',
    variant: 'link_button_banner_home_page_right',
    link: '#/',
  },
]
const DataGameRight = [
  {
    title: 'Navination Drops',
    sxBox: {
      left: '0',
      transform: ['translate(0px,330px)',
        'translate(0px,365px)', 'translate(160px,110px)',
        'translate(215px,110px)'],
      background: ['#0000009e', '#0000009e', 'none'],
      padding: ['10px', 'none'],
      width: ['155px', '155px', 'auto', 'auto']
    },
    sxText: {
      transform: 'translate(18px,-4px)',
      textAlign: 'left',
      fontSize: ['16px', '16px', '20px', '28px'],
    },
    sxImg: {
      position: 'relative', width: ['45px', '45px', '90px', '110px'],
      maxWidth: '110px'
    },
    image: 'https://cdn-front-static.dmarket.com/prod/v1-185-0/assets/img/home/navi-home.svg',
  },
  {
    title: 'Maincasts Drops',
    sxBox: {
      left: '0',
      transform: ['translate(-155px,330px)',
        'translate(-155px,365px)', 'translate(210px,240px)', 'translate(300px,240px)'],
      background: ['#0000009e', '#0000009e', 'none'],
      padding: ['10px', '10px', '13px', 'none'],
      width: ['155px', '155px', '210px', 'auto']
    },
    sxText: {
      textAlign: 'left',
      transform: 'translate(18px,-4px)',
      fontSize: ['16px', '16px', '20px', '28px'],
    },
    sxImg: {
      width: ['25px', '25px', '38px', '65px'],
      maxWidth: '145px'
    },
    image: 'https://cdn-front-static.dmarket.com/prod/v1-183-14/assets/img/home/maincast-home.svg',
  }
]
const DataGameLeft = [
  {
    title: 'RUST',
    sxBox: {
      left: ['calc(100% - 70px)', '0', '0', '0'],
      top: ['30px', '0'],
      transform: ['translate(0)', 'translate(125px)', 'translate(-245px,40px)', 'translate(-345px, 40px)'],
      width: ['140px', 'auto'],
      flexDirection: ['column', 'none'],
    },
    sxText: {
      transform: ['translate(24px,-6px)', 'translate(24px,5px)', 'translate(-45px,35px)', 'translate(-45px,35px)'],
      fontSize: ['12px', '14px', '20px', '20px'],
    },
    sxImg: {
      width: ['65px', '100px', '135px', '145px'],
      maxWidth: '145px'
    },
    image: 'https://cdn-front-static.dmarket.com/prod/v1-185-0/assets/img/home/games-rust.png',
  },
  {
    title: 'TEAM FORTRESS',
    sxBox: {
      left: ['calc(100% - 120px)', '0', '0', '0'],
      transform: ['0', 'translate(100px)', 'translate(-380px,100px)', 'translate(-496px,104px)'],
      fontSize: '24px',
      flexDirection: ['column', 'none'],
      top: ['135px', '135px', '0px', '0px'],
    },
    sxText: {
      fontSize: ['12px', '14px', '25px', '18px'],
      transform: ['translateY(0px)', 'translate(0px)', 'translate(25px,110px)', 'translate(-45px,45px)'],
      maxWidth: ['80px', '100px', '125px', '125px'],
      textAlign: ['right', 'right', 'right']
    },
    sxImg: {
      width: ['160px', '160px', '215px', '265px'],
      maxWidth: '265px',
      transform: ['scaleX(-1)', 'scaleX(-1)', 'none', 'none'],
    },
    image: 'https://cdn-front-static.dmarket.com/prod/v1-183-14/assets/img/home/games-tf2.png',
  },
  {
    title: 'CSGO',
    sxBox: {
      left: ['-20', '0'],
      transform: ['translate(0)', 'translate(-252px)', 'translate(-420px,230px)', 'translate(-487px,245px)'],
      fontSize: '36px',
      top: ['20px', '20px', '20px', '0px'],
      flexDirection: ['column', 'none'],
      width: ['140px', '140px', '160px', 'auto']
    },
    sxText: {
      transform: ['translate(40px,3px)', 'translate(40px,3px)', 'translate(55px,90px)', 'translate(-15px,80px)'],
      fontSize: ['12px', '14px', '30px', '30px']
    },
    sxImg: {
      width: ['100px', '100px', '170px', '220px'],
      maxWidth: '220px'
    },
    image: 'https://cdn-front-static.dmarket.com/prod/v1-183-14/assets/img/home/games-csgo.png',
  },
  {
    title: 'DOTA2',
    sxBox: {
      left: ['-80px', '0'],
      transform: ['translate(0)', 'translate(-345px, 50px)', 'translate(-555px,216px)', 'translate(-655px,390px)'],
      fontSize: '24px',
      top: ['150px', '150px', '150px', '0px'],
      flexDirection: ['column', 'none'],
      width: ['140px', 'auto'],
    },
    sxText: {
      transform: ['translate(100px,-20px)',
        'translate(110px,0px)', 'translate(210px,195px)',
        'translate(148px,100px)'],
      fontSize: ['12px', '14px', '24px', '24px'],
    },
    sxImg: {
      width: ['220px', '220px', '380px', '430px'],
      maxWidth: '430px'
    },
    image: 'https://cdn-front-static.dmarket.com/prod/v1-183-14/assets/img/home/games-dota2.png',
  },
]
const DataNumber = [
  {
    title: 'skins on the market',
    number: '1 000 000+',
  },
  {
    title: 'transactions recorded in blockchain NFT',
    number: '40 000 000+',
  },
  {
    title: 'closed deals',
    number: '10 000 000+',
  },
]
export const BannerHome = () => {

  return (
    <Box>
      <Box variant='box__background_homepage'>
        <Text as='h2' variant='text_cap_title'>CASHOUTS / DEPOSITS</Text>
        <SvgPaymentsIcon />
        <Caption />
        {DataGameRight.map((item, index) => (
          <GameBannerRight key={index} title={item.title} sxBox={item.sxBox}
            sxText={item.sxText} sxImg={item.sxImg} image={item.image} />
        ))}

        {DataGameLeft.map((item, index) => (
          <GameBannerLeft key={index} title={item.title} sxBox={item.sxBox}
            sxText={item.sxText} sxImg={item.sxImg} image={item.image} />
        ))}
        <Flex variant='flex_total_number'>
          {DataNumber.map((item, index) => (
            <Box key={index} variant='box__numberItem'>
              <Text variant='text_numberItem'>
                {item.number}
              </Text>
              <Text variant='text_numberTitle'>{item.title}</Text>
            </Box>
          ))
          }
        </Flex>
        <Flex variant='flex_button_banner_home_page'>
          {DataButton.map((item, index) => (
            <CommonButton key={index} variant={item.variant} link={item.link} title={item.title} />
          ))
          }
        </Flex>
      </Box>
    </Box >
  )
}
