import React, { useCallback, useEffect, useMemo } from 'react'
import { useParams } from 'react-router-dom'
import { PageContent } from '../../component/Content/pagecontent'
import useItemsContext from '../items/logic/providers'
import { IItemsContextProps } from '../items/type'
import { Box, Flex } from 'rebass'
import { Item } from '../items/ui/item'
import { Loading } from '../../component/loading'

const noteworthyData = [
  {
    id: 'topsellers',
    title: 'Top sellers',
    endpoint: 'topSellerItems'
  },
  {
    id: 'trending',
    title: 'New and trending',
    endpoint: 'newAndTrendingItems'
  },
  {
    id: 'specials',
    title: 'Current Specials',
    endpoint: 'specialCurrentItems'
  },
  {
    id: 'upcoming',
    title: 'Upcoming releases',
    endpoint: 'upcomingReleaseItems'
  }
]

export const NewNoteworthy: React.FC = () => {
    const param = useParams<{[key: string]: string}>()
    const dataFilter = useMemo(() => noteworthyData.filter((data) => data.id === param.filter), [param])

    const {itemsByEndpoint, getItemsByEndpoint, loadingByEndpoint} : IItemsContextProps = useItemsContext()

    useEffect(() => {
      if (dataFilter?.length <= 0) return
      getItemsByEndpoint(dataFilter[0].endpoint, {})
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [param.filter])

    const RenderItems = useCallback(() => {
      if (!Object.keys(itemsByEndpoint)?.length) {
        return <Box>There is no item that you are looking for.</Box>
      }

      return (
        <Flex sx={{
          flexWrap: 'wrap',
          mx: '-5px'
        }}>
          {Object.values(itemsByEndpoint).map((each) => {
            return(
              <Box sx={{
                width: ['50%', '33.33333333%', '33.33333333%', '20%'],
                p: '0 5px',
                mb: '15px'
              }} key={each.itemID}>
                <Item item={each} />
              </Box>
            )})}
        </Flex>
      )
    }, [itemsByEndpoint])

    return (
      <>
        <PageContent title={dataFilter[0].title} description_Item={'All Items'}/>
        <Box variant='box__home_container' sx={{mt: '25px'}}>
          {loadingByEndpoint && <Loading/>}
          {!loadingByEndpoint && <RenderItems/>}
        </Box>
      </>
    )
}