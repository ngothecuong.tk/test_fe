import { Box, Text } from 'rebass'
import useAuthContext from '../auth/logic/provider'
import { IAuthUser } from '../auth/types'

const Texts = [
  {
    title: ' Create a personalized profile',
  },
  {
    title: ' Easily play game with your friends',
  },
  {
    title: 'Create & join groups',
  },
  {
    title: 'Use chat room',
  },
  {
    title: ' Access SNT from with any game',
  },
  {
    title: 'Track game play status',
  },
]

export const ProFile: React.FC = () => {
  const { userInfo }: IAuthUser = useAuthContext()

  return (
    <Box variant='box_all_profile'>
      <Box variant='box_all_children_profile'>
        <Box sx={{width: ['100%', '83vw', '83vw', '820px'], paddingLeft: ['25px', '0px', '0px', '0px']}}>
        <Box variant='box_full_name_profile'>{`${userInfo?.displayName}`}</Box>
        <Text variant='text_caption_profile'>Bienvenue dans la communauté Game</Text>
        </Box>
      </Box>
      <Box variant='box_body_profile'>
        <Box variant='box_body_left_profile'>
          <Text variant='text_body_left_profile'>Captions</Text>
          <Text variant='text_body_left_note'>
            Vous pouvez désormais communiquer avec vos contacts tout en jouant. La communauté Steam
            vous permet de prendre des nouvelles de vos contacts, de voir qui joue, de lancer ou
            rejoindre une partie ou tout simplement de discuter. Avec plus de 100&nbsp;millions
            d'utilisateurs et utilisatrices Steam à travers le monde, vous n'aurez aucun mal à faire
            de nouvelles connaissances.
          </Text>
        </Box>
        <Box variant='box_body_right_profile'>
          <Box sx={{width: ['auto', '307px', '307px', 'auto'], marginTop: ['0px', '0px', '0px', '45px']}}>
          <Text sx={{ fontSize: '20px', color: ' #0080ff' }}>Get Started Now!</Text>
          <Text sx={{ marginTop: '5px', fontSize: '14px', color: '#9099a1'}}
           >
            Setup your profile, start a group and invite some friends!
          </Text>
          <Box>
          <hr />
          </Box>
          <Box sx={{ fontWeight: '700' }}>The End...</Box>
          {Texts.map((item, index) => {
            return (
              <Text key={index} variant='text_href_profile'>
                <span> &#62;&#62; </span>
                {item.title}
              </Text>
            )
          })}
          </Box>
        </Box>
      </Box>
    </Box>
  )
}