import React, { createContext, useContext, useState} from 'react'
import { IAuthUser} from '../types'

export const AuthContext = createContext<IAuthUser>(null!)
export const AuthProvider: React.FC = ({ children }) => {
  const [userInfo, setUserInfo] = useState({userID: ''})
  const [loading, setLoading] = useState(false)

   return (
    <AuthContext.Provider
      value={{
        userInfo,
        setUserInfo,
        loading,
        setLoading,
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}

export default function useAuthContext() {
  const context = useContext(AuthContext)
  if (!context) {
    throw new Error('authContext must be used within a AuthProvider')
  }

  return context
}
