export interface IAuthUser {
  userInfo: IUserInfo
  setUserInfo: (userInfo: IUserInfo) => void
  loading: boolean
  setLoading: (isLoading: boolean) => void
}
export interface IAccess {
  companyID?: string
  companyIDAndUserID?: string
  createdAt?: string
  createdBy?: string
  departmentID?: string
  role?: string
  status?: string
  updateAt?: string
  userID?: string
  _id?: string
}
export interface IPermissions {
  createdBy?: string
  companyID?: string
  projectID?: string
  roleName: string
  status: string
  userID?: string
  _id?: string
}
export interface IUserInfo {
  avatar?: string
  country?: string
  displayName?: string
  gender?: string
  phoneNumber?: number
  status?: string
  userID: string
  permissions?: IPermissions
  access?: IAccess
}
