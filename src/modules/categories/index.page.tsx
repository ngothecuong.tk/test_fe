import { useEffect, useMemo } from 'react'
import { useParams } from 'react-router-dom'
import { Box } from 'rebass'
import { Breadcrumbs } from '../../component/breadcrumb'
import { Loading } from '../../component/loading'
import { FeaturedRecommend } from '../home/ui/featured_recommend'
import { TabLists } from '../home/ui/tab_list'
import useCategoriesContext from './logic/provider'
import { ICategory, ICategoryContext } from './types'

interface IParamData {
  [key: string]: ICategory
}

type Params = { [K in keyof IParamData]: string }
export const Category: React.FC = () => {
  const { categories, getCategoriesAction, loadingCategories }: ICategoryContext = useCategoriesContext()
  const param = useParams<Params>()
  const categoriFind = useMemo(
    () => Object.values(categories).find((category) => category.categoryID === param.categoryID),
    [categories, param.categoryID]
  )

  const breadcrumbData = [
    {
      name: 'Home',
      link: '/',
    },
    {
      name: 'All project',
      link: '/projects'
    },
    {
      name: `${categoriFind?.name}`,
    },
  ]
  useEffect(() => {
    if (param?.categoryID === '') return
    getCategoriesAction(param?.categoryID)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [param])

  return (
    <>
    <Breadcrumbs breadCrumbData={breadcrumbData} />
      {loadingCategories ? (
        <Loading />
      ) : (
        <Box>
          <FeaturedRecommend />
          <TabLists/>
        </Box>
      )}
    </>
  )
}
