export interface ICategory {
    categoryID: string
    name: string
    description?: string
    icon?: string
    createAt: string
    updatedAt: string
}
export interface ICategoryContext {
    categories: {[key: string]: ICategory}
    setCategories: (categories: {[key: string]: ICategory}) => void
    getCategoriesAction: (categoryID: string) => void
    loadingCategories: boolean
    setLoadingCategories: (loadingCategories: boolean) => void
}
