import { createContext, useCallback, useContext, useState } from 'react'
import { IResponseDataType } from '../../../types'
import { getCategories } from './api'
import { ICategory, ICategoryContext } from '../types'

export const categoriesContext = createContext<ICategoryContext>(null!)
export const CategoriesProvider: React.FC = ({ children }) => {
  const [categories, setCategories] = useState({})
  const [loadingCategories, setLoadingCategories] = useState(false)

  const getCategoriesAction = useCallback(() => {
    setLoadingCategories(true)
    getCategories()
      .then((response: IResponseDataType<ICategory>) => {
        setCategories(response.list)
        setLoadingCategories(false)
      })
      .catch((error) => {
        throw error
      })
  }, [])

  return (
    <categoriesContext.Provider
      value={{
        categories,
        setCategories,
        getCategoriesAction,
        loadingCategories,
        setLoadingCategories,
      }}
    >
      {children}
    </categoriesContext.Provider>
  )
}
export default function useCategoriesContext() {
  const context = useContext(categoriesContext)
  if (!context) {
    throw new Error('useCategoriesContext must be used within a CategoriesProvider')
  }

  return context
}
