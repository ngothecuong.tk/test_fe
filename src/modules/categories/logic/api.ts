import { ultis } from 'react-cores'

export const getCategories = async () => {
    try {
          const res = await ultis.api({
              path: 'categories',
              method: 'GET',
        })
        const formatData = res?.data.list

          return {
              list: formatData
          }
    }
    catch (error) {
     throw error
    }

}
