import './App.css';
import { ThemeProvider } from 'emotion-theming'
import { hoc, ultis } from 'react-cores'
import { routes } from './routes';
import { providers } from './providers';
import { ErrorPage } from './modules/error/index.page';
import {baseConfig} from './ultis/base_config';
import { theme } from './theme'
import {Layout} from './component/layout'

ultis.firstSetup(baseConfig)

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className='App'>
        <hoc.SettingHOC
          routes={routes}
          ErrorPage={ErrorPage}
          providers={providers}
          baseConfig={baseConfig}
          Layout={Layout}
          onUserInfo={(result, context) => {
            // need to set result, what is response from endpoint users/me in baseConfig, to provider
            if (result?.status !== 200){
              return
            }
            context?.setUserInfo(result?.data)
            // context?.setUser(result)
          }
        }
        />
      </div>
    </ThemeProvider>
  );
}

export default App;
