export const link = {
    link_common_button: {
        background: 'linear-gradient(90deg, #06BFFF 0%, #2D73FF 100%)',
        boxShadow: '0 0 4px #000',
        textAlign: 'center',
        height: '58px',
        fontSize: '16px',
        width: ['48%', '48%', '48%', '24%'],
        color: '#fff',
        fontWeight: '600',
        margin: ['3px'],
        paddingTop: ['20px'],
        cursor: 'pointer',
        fontFamily: '"Motiva Sans", Sans-serif;',
        textDecoration: 'none ',
        '&:hover': {
            background: 'linear-gradient(90deg,#61dafb 0%,#2D73FF 100%)'
        }
    },
    link_button_banner_home_page_left: {
        width: ['145px', '145px;', '200px', '250px'],
        height: ['45px', '45px', '60px', '70px'],
        margin: ['20px 8px 0', '35px 8px 0'],
        fontSize: ['12px', '12px', '18px', '18px'],
        fontWeight: '800',
        textTransform: 'uppercase',
        color: '#fff',
        paddingTop: ['17px', '17px', '20px', '26px'],
        textDecoration: 'none ',
        borderRadius: '5px',
        background: 'linear-gradient(253.3deg,#f0965d,#f5d673)',
        '&:hover': {
            background: 'linear-gradient(253.3deg,#f0965d,#f08441)',
        },
    },
    link_button_banner_home_page_right: {
        width: ['145px', '145px;', '200px', '250px'],
        height: ['45px', '45px', '60px', '70px'],
        margin: ['20px 8px 0', '35px 8px 0'],
        fontSize: ['12px', '12px', '18px', '18px'],
        fontWeight: '800',
        textTransform: 'uppercase',
        color: '#fff',
        paddingTop: ['17px', '17px', '20px', '26px'],
        textDecoration: 'none ',
        borderRadius: '5px',
        background: 'linear-gradient(253.3deg,#75cbbe,#63ca7c)',
        '&:hover': {
            background: 'linear-gradient(253.3deg,#75cbbe,#44a569)',
        }
    },
    link_signin: {
        color: ['#fff', 'black', 'black', 'black'],
        paddingLeft: '5px',
        textDecoration: 'none',
        height: '42px',
        display: ['flex', 'none', 'none', 'none'],
        backgroundColor: 'rgba(0, 0, 0, .07)',
        borderRadius: '50px',
        cursor: 'pointer',
        border: '1px solid #eceff1',
        boxShadow: '0 1px 2px 1px rgba(0, 0, 0, .06)',
        alignItems: 'center',
        justifyContent: 'center',
        width: '85%',
        ':hover': {
            backgroundColor: 'rgba(255, 255, 255, .02)',
        }
    },
    link_signin_no_responsive: {
        color: 'black',
        paddingLeft: '5px',
        textDecoration: 'none',
        fontWeight: '700',
        background: '#fff',
        borderRadius: '50px',
        height: '42px',
        width: ['100%', '95%', ' 80%', '60%'],
        border: '1px solid #eceff1',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        ':hover': {
            boxShadow: '0 1px 2px 1px rgba(0, 0, 0, .06)',
        },
        marginLeft: ['0px', '0px', '0px', '15px']
    },
    link__menu_submenu_item: {
      display: ['block', 'block', 'flex', 'flex'],
      color: '#b8b6b4',
      marginLeft: ['0', '0', '10px', '10px'],
      textDecoration: 'none',
      fontSize: ['22px', '22px', '16px', '16px'],
      paddingTop: ['10px', '10px', '0px', '0px'],
      paddingBottom: ['10px', '10px', '0px', '0px'],
      paddingLeft: ['20px', '20px', '0', '0'],
      paddingRight: ['20px', '20px', '0', '0'],
      borderBottom: ['1px solid #2f3138', '1px solid #2f3138', 'none', 'none'],
      cursor: 'pointer',
      ':hover': {
        color: '#FFFFFF',
      },
    },
    link_hover_submenu: {
        margin: '5px',
        cursor: 'pointer',
        position: 'relative',
        color: '#d9d9d9',
        fontFamily: 'system-ui',
        fontSize: '14px',
        borderBottom: '2px solid transparent',
        textDecoration: 'none',
        ':hover': {borderBottom: '2px solid #d9d9d9', width: 'fit-content'}
    },
    link_hover_subMenu2: {
        cursor: 'pointer',
        color: '#d9d9d9',
        fontFamily: 'cursive',
        fontSize: '16px',
        padding: '10px',
        width: '100%'
      },
    link_content_bot: {
        color: '#8F98A0',
        fontSize: '14px',
        marginTop: '10px',
        display: 'flex',
        flexDirection: 'column',
        fontFamily: '"Motiva Sans", sans-serif',
      }
}
