export const button = {
  button__default: {
    backgroundColor: '#F00',
    backgroundImage: 'none',
    color: 'red',
  },
  button__rounded: {
    varian: 'button__default',
    borderRadius: '100px'
  },
  button_browse: {
    background: 'linear-gradient(90deg, #06BFFF 0%, #2D73FF 100%)',
    boxShadow: '0 0 4px #000',
    textAlign: 'center',
    fontWeight: '600px !important',
    color: '#FFFFFF',
    height: '58px',
    fontSize: '16px',
    width: ['48%', '48%', '48%', '24%'],
    cursor: 'pointer',
    fontFamily: '"Motiva Sans", Sans-serif;',
    textDecoration: "'none ' !important",
    '&:hover': {
      background: 'linear-gradient(90deg,#61dafb 0%,#2D73FF 100%)'
    },
  },
  button__slider_left: {
    background: 'linear-gradient( to right, rgba( 0, 0, 0, 0.3) 5%,rgba( 0, 0, 0, 0) 95%)',
    color: '#fff',
    width: ['none', 'none', '45px', '45px'],
    height: ['none', 'none', '108px', '108px'],
    p: '25px 10px',
    borderRadius: '0',
    zIndex: 100,
    display: ['none!important', 'none!important', 'none!important', 'block!important'],
    '&::before': {
      display: 'none'
    },
    '&:focus': {
      background: 'linear-gradient( to right, rgba( 0, 0, 0, 0.3) 5%,rgba( 0, 0, 0, 0) 95%)',
      color: 'white',
    },
    '&:hover': {
      background: 'linear-gradient( to right, rgba( 171, 218, 244, 0.3) 5%,rgba( 171, 218, 244, 0) 95%)',
      color: '#fff'
    },
  },
  button__slider_right: {
    background: 'linear-gradient( to right, rgba( 0, 0, 0, 0) 5%,rgba( 0, 0, 0, 0.3) 95%)',
    color: '#fff',
    width: ['none', 'none', '45px', '45px'],
    height: ['none', 'none', '108px', '108px'],
    p: '25px 10px',
    borderRadius: '0',
    zIndex: 100,
    display: ['none!important', 'none!important', 'none!important', 'block!important'],
    '&::before': {
      display: 'none'
    },
    '&:focus': {
      background: 'linear-gradient( to right, rgba( 0, 0, 0, 0) 5%,rgba( 0, 0, 0, 0.3) 95%)',
      color: 'white',
    },
    '&:hover': {
      background: 'linear-gradient( to right, rgba( 171, 218, 244, 0) 5%,rgba( 171, 218, 244, 0.3) 95%)',
      color: '#fff'
    },
  },
  button__add_to_cart: {
    position: 'absolute',
    top: '0',
    right: '0',
    background: '#4c6b22',
    color: '#a4d007',
    p: '4px',
    fontSize: '13px',
    '&:hover': {
      color: '#fff',
      cursor: 'pointer'
    }
  },
  button_menu_cart: {
    height: '25px',
    display: 'flex',
    background: '#5c7e10',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    cursor: 'pointer',
    color: '#e5e4dc',
    borderRadius: '0px'
  }
}