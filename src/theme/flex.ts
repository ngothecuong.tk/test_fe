export const flex = {
    flex_button_banner_home_page: {
        display: 'flex',
        maxWidth: '750px',
        margin: '0 auto',
        justifyContent: 'center',
        flexWrap: 'wrap',
        position: 'sticky',
        zIndex: '1',
    },
    flex_total_number: {
        display: 'flex',
        margin: '0 auto',
        justifyContent: 'center',
        flexWrap: 'wrap',
        position: 'sticky',
        zIndex: 10,
        width: ['320px', '320px', '420px', '670px']
    }
}