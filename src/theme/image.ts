export const image = {
  image__default: {
    maxWidth: '100%',
    height: 'auto'
  },
  image_banner_hover: {
    opacity: '0',
    top: '-3px',
    left: '0px',
    position: 'absolute',
    height: '100%',
    '&:hover': {
      opacity: '1'
    }
  },
  image_item_project: {
    transitionDuration: '0.4s',
    transitionTimingFunction: 'cubic-bezier(0, 0.73, 0.48, 1)',
    borderRadius: '5px',
    objectFit: 'cover',
    width: ['150px', '130px', '145px', '128px'],
    height: ['150px', '130px', '145px', '128px'],
  },
  image_item_project_child: {
    maxWidth: '16px',
    width: '100%',
    height: '16px',
    p: '2px 6px 2px 0px',
    boxSizing: 'content-box',
  }
}