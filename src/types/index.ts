export interface IHomeContextProps {
  title: string
  setTitle: (title: string) => void
}
export interface IResponseDataType<Type> {
  list: {[key: string]: Type}
}