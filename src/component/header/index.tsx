import { Box } from 'rebass'
import HeaderGlobal from './UI/header_global'

export const HeaderUI = () => {
  return (
    <Box>
      <HeaderGlobal />
    </Box>
  )
}
