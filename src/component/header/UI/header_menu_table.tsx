import { Box, Link } from 'rebass'
import { Language } from './language'
import { Loginheader } from './login_header'

export const TableHeader: React.FC = () => {
  return (
    <Box variant='box__menu_table'>
      <Box sx={{ width: '100%' }}>
        <Box variant='box__login' href='/signin'>
          <Loginheader />
        </Box>
        <Box
          sx={{
            boxShadow: ['0 0 5px 4px black', '0 0 5px 4px black', 'none', 'none'],
            height: '100vh',
          }}
        >
          <Link variant='link__menu_submenu_item'>Store</Link>
          <Link variant='link__menu_submenu_item'>Community</Link>
          <Link variant='link__menu_submenu_item'>Support</Link>
          <Box variant='box__language'>
            <Language />
          </Box>
        </Box>
      </Box>
    </Box>
  )
}
export default TableHeader
