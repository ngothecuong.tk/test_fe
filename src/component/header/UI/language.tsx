import { Box, Text } from 'rebass'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretDown } from '@fortawesome/free-solid-svg-icons'
import { useState, useCallback } from 'react'
interface IInitialProps {
  children?: React.ReactNode
}
const arrlanguge = [
  {
    name: 'Vietnamese',
  },
  {
    name: 'English',
  },
  {
    name: 'Janpanese',
  },
  {
    name: 'Chinaese',
  },
  {
    name: 'Malaysiaese',
  },
]
export const Language: React.FC<IInitialProps> = () => {

  const [isDrop, setIsDrop] = useState(false)
  const [selectDrop, setSelectDrop] = useState<string>('Language')

  const handleSortByItem = useCallback((name: string) => {
    setSelectDrop(name)
    setIsDrop(false)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Box
      sx={{
        width: ['250px', '250px', '120px', '120px'],
        position: 'relative',
      }}
    >
      <Box
        variant='box_select__droplanguage'
        onClick={(e) => {
          e.stopPropagation()
          setIsDrop(!isDrop)
        }}
      >
        <Text
          sx={{
            marginRight: '8px',
            paddingLeft: ['20px', '20px', '0px', '0px'],
            fontSize: ['16px', '16px', '14px', '14px'],
            ':hover': {
              color: '#FFFFFF',
            },
          }}
        >
          {selectDrop}
        </Text>
        <Box sx={{ fontSize: ['22px', '22px', '16px', '16px'], marginRight: '9%' }}>
          <FontAwesomeIcon icon={faCaretDown} style={{ fontSize: 'inherit' }} />
        </Box>
      </Box>
      {isDrop && (
        <Box>
          <Box variant='box_select__listlanguage'>
            {arrlanguge.map((item, index) => (
              <Box
                variant='box_select__listItemlanguage'
                key={index}
                sx={{
                  backgroundColor:
                    selectDrop === item.name ? ['none', 'none', '#171a21', '#171a21'] : '',
                  color: selectDrop === item.name ? '#FFFFFF' : '',
                }}
                onClick={() => handleSortByItem(item.name)}
              >
                <Text
                  sx={{
                    ':hover': {
                      color: '#FFFFFF',
                    },
                  }}
                >
                  {item.name}
                </Text>
              </Box>
            ))}
          </Box>
          <Box
            sx={{
              top: '0',
              visibility: 'visible',
              width: '100%',
              height: '100vh',
              position: 'fixed',
              left: '0',
              zIndex: 1,
            }}
            onClick={() => {
              setIsDrop(false)
            }}
          />
        </Box>
      )}
    </Box>
  )
}
