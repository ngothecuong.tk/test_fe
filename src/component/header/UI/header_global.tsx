import { useState } from 'react'
import { Box, Flex, Link, Text } from 'rebass'
import { Language } from './language'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { TableHeader } from './header_menu_table'
import { Loginheader } from './login_header'

const arrLink = [
  {
    name: 'STORE',
  },
  {
    name: 'COMMUNITY',
  },
  {
    name: 'ABOUT',
  },
  {
    name: 'SUPPORT',
  },
]
const HeaderGlobal: React.FC = () => {
  const [toggle, setToggle] = useState(false)

  return (
    <Box variant='box__menu'>
      <Box variant='box__menu_content'>
        <Box
          sx={{
            width: ['100%', '100%', '200px', '200px'],
            paddingTop: ['20px', '20px', '20px', '0px'],
          }}
        >
          <Box variant='box__icon'>
            <FontAwesomeIcon
              color={'#fff'}
              icon={faBars}
              onClick={() => {
                setToggle(!toggle)
              }}
            />
          </Box>
          <Link>
            <Text variant='text__logo' sx={{ width: '185px', margin: 'auto' }}>
              Logo Gaming
            </Text>
          </Link>
        </Box>
        <Box variant='box__menu_submenu'>
          {arrLink.map((item, index) => (
            <Link variant='link__menu_submenu_item' key={index}>
              {item.name}
            </Link>
          ))}
        </Box>
        <Box variant='box__menu_action'>
          <Text
            sx={{
              color: '#b8b6b4',
              width: ['none', 'none', '150px', '150px'],
              textAlign: 'right',
              paddingRight: ['0px', '0px', '8px', '8px'],
              borderRight: '1px solid #fff',
              ':hover': {
                color: '#FFFFFF',
              },
            }}
          >
            <Loginheader />
          </Text>

          <Language />
        </Box>
      </Box>
      {toggle && (
        <Flex>
          <TableHeader />
          <Box
            sx={{
              backgroundColor: 'rgba( 0, 0, 0, 0.4)',
              top: '0',
              visibility: 'visible',
              width: '100%',
              height: '100vh',
              position: 'fixed',
              zIndex: 1,
            }}
            onClick={() => {
              setToggle(false)
            }}
          />
        </Flex>
      )}
    </Box>
  )
}
export default HeaderGlobal
