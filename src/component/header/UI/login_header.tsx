import { faCaretDown } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useCallback, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Box, Text, Link } from 'rebass'
import useAuthContext from '../../../modules/auth/logic/provider'
import { IAuthUser } from '../../../modules/auth/types'

type Token = string | null
interface IInitialProps {
  children?: React.ReactNode
}

export const Loginheader: React.FC<IInitialProps> = () => {
  const [isDrop, setIsDrop] = useState(false)
  const { userInfo }: IAuthUser = useAuthContext()
  const token: Token = localStorage.getItem('access_token')
  const displayName: string = userInfo.displayName === undefined ? '' : userInfo.displayName
  const history = useHistory()
  const [tablelogin, setTablelogin] = useState('')

  const logOut = useCallback(() => {
    localStorage.removeItem('access_token')
    history.replace('/signin')
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Box
      sx={{
        paddingLeft: ['20px', '20px', '0', '0'],
        paddingTop: ['4px', '4px', '0px', '0px'],
        position: 'relative',
      }}
    >
      {token ? (
        <Box
          variant='box_login__drop'
          onClick={(e) => {
            e.stopPropagation()
            setIsDrop(!isDrop)
          }}
        >
          <Box
            onClick={() => setTablelogin(tablelogin === '' ? 'text_login' : '')}
            variant={`${tablelogin}`}
            sx={{ display: 'flex', alignItems: ['none', 'none', 'center', 'center'] }}
          >
            <Text
              sx={{
                width: '200px',
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                height: ['none', 'none', '16px', '16px'],
                marginRight: ['0px', '0px', '8px', '8 px'],
                fontSize: ['22px', '22px', '14px', '14px'],
                ':hover': {
                  color: '#FFFFFF',
                },
              }}
            >
              {displayName}
            </Text>
            <Box sx={{ fontSize: ['22px', '22px', '16px', '16px'] }}>
              <FontAwesomeIcon icon={faCaretDown} style={{ fontSize: 'inherit' }} />
            </Box>
          </Box>
        </Box>
      ) : (
        <Box>
          <Link href='/signin'>
            <Text
              sx={{
                display: 'inline-block',
                color: '#b8b6b4',
                fontSize: ['22px', '22px', '14px', '14px'],
                ':hover': {
                  color: '#FFFFFF',
                },
              }}
            >
              Login
            </Text>
          </Link>
        </Box>
      )}

      {isDrop && (
        <Box variant='box_login__list'>
          <Box variant='box_login__listItem'>
            <Link href='/profile'>
              <Text
                sx={{
                  color: '#b8b6b4',
                  display: 'inline-block',
                  width: ['200px', '200px', '100%', '100%'],
                  fontSize: ['16px', '16px', '14px', '14px'],
                  ':hover': {
                    color: '#FFFFFF',
                  },
                }}
              >
                View profile
              </Text>
            </Link>
            <Text
              onClick={logOut}
              sx={{
                fontSize: ['16px', '16px', '14px', '14px'],
                cursor: 'pointer',
                color: '#57cbde',
              }}
            >
              Logout
            </Text>
          </Box>
          <Box
            sx={{
              top: '0',
              visibility: 'visible',
              width: '100%',
              height: '100vh',
              position: 'fixed',
              zIndex: '-1',
              left: '0',
              display: ['none', 'none', 'block', 'block'],
            }}
            onClick={() => {
              setIsDrop(false)
            }}
          />
        </Box>
      )}
    </Box>
  )
}
