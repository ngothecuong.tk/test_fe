import { Box, Text, Flex } from 'rebass'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretDown } from '@fortawesome/free-solid-svg-icons'
import { useCallback, useEffect, useState } from 'react'
import { Link, useHistory, useLocation } from 'react-router-dom'
import { IProjectsContextProps } from '../../modules/projects/type/index'
import useProjectsContext from '../../modules/projects/logic/providers'
import { convertStringToObjectUrl } from '../../helpers/convert_between_object_string'
import { setQuery } from '../../helpers/setQuery'

// const sortBys = ['companyID', 'name', 'createdAt', 'projectID']
interface IProps {
  children?: React.ReactNode
}
const sortData = [
  {
    label: 'Default',
    value: 'default',
    link: '/projects',
  },
  {
    label: 'Name',
    value: 'name',
    link: '/projects',
  },
  {
    label: 'Category',
    value: 'Category',
  },
]

export const SortBy: React.FC<IProps> = () => {
  const [isDrop, setIsDrop] = useState(false)
  const [selectDrop, setSelectDrop] = useState<string>('Default')
  const { getProjects }: IProjectsContextProps = useProjectsContext()
  const location = useLocation()
  const queryObject = convertStringToObjectUrl(location.search)
  const queryname = String(queryObject?.name)
  const history = useHistory()
  const [sortByValue, setSortByValue] = useState<string>('')

  useEffect(() => {
    if (!String(queryObject?.sortBy) || String(queryObject?.sortBy) === 'Default') {
      return
    }

    getProjects({ sortBy: sortByValue, sortDirection: 'ASC', name: queryname })
  }, [queryObject?.sortBy])

  const handleSortBy = useCallback((value: string) => {
    setSelectDrop(value)
    if (!value || value === 'Default') {
      return
    }

    const projectQuery = setQuery({
      name: queryname as string,
      sortBy: value as string,
    })
    history.replace({
      pathname: history.location.pathname,
      search: projectQuery || '',
    })
  }, [])

  return (
    <Flex
      sx={{
        float: ['left', 'left', 'right', 'right'],
      }}
    >
      <Text m={['9px 9px 9px 0', ' 9px 9px 9px 0', ' 9px', ' 9px']} color='#4c6c8c'>
        Sort By
      </Text>
      <Box width='170px' sx={{ position: 'relative' }}>
        <Box
          variant='box_select__drop'
          onClick={(e) => {
            e.stopPropagation()
            setIsDrop(!isDrop)
          }}
        >
          <Text>{selectDrop}</Text>
          <FontAwesomeIcon icon={faCaretDown} style={{ marginLeft: 'auto' }} />
        </Box>
        {isDrop && (
          <Box variant='box_select__list'>
            {sortData.map((item, index) => (
              <Link
                key={item.value}
                to={`${item.link}`}
                style={{ color: '#ffffff', textDecoration: 'none' }}
              >
                <Box
                  variant='box_select__listItem'
                  key={index}
                  style={{
                    background:
                      selectDrop === item.label
                        ? 'linear-gradient( -60deg, #417a9b 5%,#67c1f5 95%)'
                        : '',
                    color: selectDrop === item.label ? '#fffffff' : '',
                  }}
                  onClick={() => {
                    setSortByValue(item.value)
                    handleSortBy(item.value)
                  }}
                >
                  {item.label}
                </Box>
              </Link>
            ))}
            <Box
              sx={{
                top: '0',
                visibility: 'visible',
                width: '100%',
                height: '100vh',
                position: 'fixed',
                left: '0',
                zIndex: -1,
              }}
              onClick={() => {
                setIsDrop(false)
              }}
            />
          </Box>
        )}
      </Box>
    </Flex>
  )
}
