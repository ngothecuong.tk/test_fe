import React from 'react'
import { Image, Text, Box } from 'rebass'
import { Search } from './search'
interface IInitialProps {
  title: string,
  description_Item: string,
}
export const PageContent: React.FC<IInitialProps> = ({ title, description_Item }) => {
  return (
    <>
      <Box backgroundColor='#000' p={['10px']}>
        <Box>
          <Box
            sx={{
              margin: '0 auto',
              maxWidth: '940px',
              position: 'relative',
            }}

          >
            <Image variant='img_banner' src='https://cdn.akamai.steamstatic.com/steam/clusters/about_i18n_assets/about_i18n_assets_0/new_features_banner_english.png?t=1636143670' />
            <br />
            <Image variant='image_banner_hover'
              src='https://cdn.akamai.steamstatic.com/store/labs/banner/labs_search_banner_hover.png' />
          </Box>
        </Box>
        <Text variant='title_Item'>
          {title}
        </Text>
        <Text variant='description_Item' >
          {description_Item}
        </Text>
        <Search />
      </Box>
    </>
  )
}