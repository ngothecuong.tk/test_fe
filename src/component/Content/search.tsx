import React, { useEffect, useState } from 'react'
import { Box, Flex, Button } from 'rebass'
import { Input } from '@rebass/forms'
import { SortBy } from './sort_by'
import { hooks } from 'react-cores'
import { IProjectsContextProps } from '../../modules/projects/type'
import useProjectsContext from '../../modules/projects/logic/providers'
import { useHistory, useLocation } from 'react-router-dom'
import { convertStringToObjectUrl } from '../../helpers/convert_between_object_string'
interface IProps {
  children?: React.ReactNode
}
export const Search: React.FC<IProps> = () => {
  const [searchName, setSearchName] = useState<string>(null!)
  const { getProjects }: IProjectsContextProps = useProjectsContext()
  const history = useHistory()
  const location = useLocation()

  useEffect(() => {
    const queryObject = convertStringToObjectUrl(location.search)
    if (queryObject?.name) {
      setSearchName(String(queryObject?.name))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  hooks.useDebounce(
    () => {
      if (searchName && searchName.length > 0) {
        getProjects({ name: searchName })
        history.push({
          pathname: history.location.pathname,
          search: `name=${searchName}`,
        })

        return false
      }
      if (searchName === '') {
        getProjects({})
        history.push({
          pathname: history.location.pathname,
        })
      }
    },
    [searchName],
    500
  )

  return (
    <>
      <Box maxWidth='940px' margin='0 auto' p='10px' backgroundColor='#101822'>
        <Flex sx={{ flexWrap: 'wrap' }}>
          <Box width={['100%', '100%', '67%', '70%']}>
            <Flex>
              <Input
                color='#fff'
                mr='10px'
                sx={{
                  border: 'solid 1px #000',
                }}
                type='text'
                placeholder='Enter search term or tag'
                value={searchName || ''}
                onChange={(e) => {
                  setSearchName(e.target.value)
                }}
              />
              <Button
                backgroundColor='rgb(103 193 245 / 20%)'
                color='#67c1f5'
                fontWeight='300'
                width='100px'
                sx={{
                  '&:hover': {
                    background: 'linear-gradient( -60deg, #417a9b 5%,#67c1f5 95%)',
                    color: '#ffffff',
                  },
                }}
              >
                Search
              </Button>
            </Flex>
          </Box>
          <Box width={['65%', '100%', '33%', '30%']} mt={['10px', '10px', '0px', '0px']}>
            <SortBy />
          </Box>
        </Flex>
      </Box>
    </>
  )
}
