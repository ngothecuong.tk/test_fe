import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCartArrowDown } from '@fortawesome/free-solid-svg-icons'
import { Button } from 'rebass'
import { useCallback } from 'react'
import { IItemVariants } from '../../modules/item_variants/type'
import { IAuthUser } from '../../modules/auth/types'
import useAuthContext from '../../modules/auth/logic/provider'
import { ICartContextProps } from '../../modules/cart/type'
import useCartContext from '../../modules/cart/logic/providers'
import { hooks } from 'react-cores'
import {postCartItemVariantsToApi} from '../../modules/cart/logic/api'
interface IInitialProps {
  item: IItemVariants
}

export const ButtonAddToCart: React.FC<IInitialProps> = ({item}) => {
  const {enqueueSnackbar}: hooks.WithSnackbarProps = hooks.useNoti()
  const {userInfo} : IAuthUser = useAuthContext()
  const { didClick, cartItemVariants, addToCart, setDidClick } : ICartContextProps = useCartContext()

  const handleAddToCart = useCallback(() => {
    const params = {
      userID: userInfo.userID,
      itemVariantID: item.itemVariantID
    }
    if (didClick) {
      // check if itemVariant is already in cart and show notification
      let isCheck = false
      for (const cartItem of Object.values(cartItemVariants)) {
        if (cartItem.itemVariantID.itemVariantID === item.itemVariantID) {
          enqueueSnackbar('The item is already in your cart', {variant: 'info'})
          isCheck = true ;
        }
      }
      // if itemVariant is not in cart. Call api and add itemVariant in cart
      if (!isCheck) {
        setDidClick(false)
        void postCartItemVariantsToApi(params)
        .then((response) => {
          if (response) {
            enqueueSnackbar('The item has been added to cart successfully', {variant: 'success'})
            addToCart(response.list, item)
          }
        })
        .catch((err) => {
          enqueueSnackbar('There is something wrong. Try again!', { variant: 'error' })
          setDidClick(true)
          throw err
        })
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [didClick])

  return (
    <Button variant='button__add_to_cart' onClick={handleAddToCart}>
      <FontAwesomeIcon
        icon={faCartArrowDown}
        style={{
          width: '13px',
          height: '13px'
        }}
      />
    </Button>
  );
}
