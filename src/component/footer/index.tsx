import { faEnvelope, faGlobe, faMobileAlt, faPhoneAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Box, Link, Text } from 'rebass'

const ContentFooter = [
  {
    title: 'New & Note worthy',
    type: '1',
    sub: [
      {
        title: 'Top Sellers',
        link: '/',
      },
      {
        title: 'New & Trending',
        link: '/',
      },
      {
        title: 'Current Specials',
        link: '/',
      },
      {
        title: 'Popular Upcoming',
        link: '/',
      },
    ],
  },
  {
    title: 'Game NFT',
    type: '2',
    sub: [
      {
        title: 'Home',
        link: '/',
      },
      {
        title: 'About us',
        link: '/',
      },
    ],
  },
  {
    title: 'Categories',
    type: '3',
    sub: [
      {
        title: 'Actions',
        link: '/',
      },
      {
        title: 'Sport & Racing',
        link: '/',
      },
      {
        title: 'Adventure & Casual',
        link: '/',
      },
      {
        title: 'Strategy',
        link: '/',
      },
      {
        title: 'Role-Playing',
        link: '/',
      },
    ],
  },
  {
    title: 'Contact',
    type: '4',
    sub: [
      {
        title: 'anna@sntsolutions.io',
        link: '/',
      },
    ],
  },
]
const IconFooter = [
  {
    icon: faEnvelope,
  },
  {
    icon: faGlobe,
  },
  {
    icon: faMobileAlt,
  },
  {
    icon: faPhoneAlt,
  },
]

export const Footer: React.FC = () => {
  return (
    <Box variant='box_all_footer'>
      <Box variant='box_all_footer_child'>
        <Box variant='box_footer_top'>
          {ContentFooter.map((item, idx) => {
            return (
              <Box variant='box_content_footer' key={idx}>
                <Box variant='box_content_top'>
                  <Box>{item.title}</Box>
                </Box>
                <Box sx={{ position: 'static' }}>
                  {item?.sub?.map((each, index) => {
                    return (
                      <Link key={index} variant='link_content_bot'>
                        {each.title}
                      </Link>
                    )
                  })}
                </Box>
              </Box>
            )
          })}
        </Box>
        <Box sx={{ width: ['95%', '75%', '85%', '940px'] }}>
          <Box variant='box_follow_us'>Follow us</Box>
          <Box variant='box_all_icon_footer'>
            {IconFooter.map((item, index) => (
              <FontAwesomeIcon
                key={index}
                icon={item.icon}
                style={{ width: '20px', height: '20px', color: '#8dd294', marginRight: '10px' }}
              />
            )
            )}
          </Box>
          <Box variant='box_text_content'>
            <Text sx={{ color: '#8F98A0', fontSize: '12px' }}>
              &copy; 2021 Valve Corporation. Tous droits réservés. Toutes les marques commerciales
              sont la propriété de leurs titulaires aux États-Unis et dans d'autres pays.
            </Text>
          </Box>
          <Box variant='box_cap_footer'>
            <Text sx={{ color: '#fff' }}>logo Game NFT</Text>
            <Text variant='text_bottom_footer'>&copy; 2022 GameNFT. All rights</Text>
            <Text variant='text_bottom_footer'>STEAM</Text>
          </Box>
        </Box>
      </Box>
    </Box>
  )
}
