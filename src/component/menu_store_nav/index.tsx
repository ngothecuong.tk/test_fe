import { Box, Flex, Button } from 'rebass'
import { useEffect, useMemo, useState } from 'react'
import { CommonMenu } from '../../component/common_menu'
import { ICategoryContext, ICategory } from '../../modules/categories/types'
import useCategoriesContext from '../../modules/categories/logic/provider'
import { useParams, useHistory } from 'react-router-dom'
import { ICartContextProps } from '../../modules/cart/type'
import useCartContext from '../../modules/cart/logic/providers'
import { IAuthUser } from '../../modules/auth/types'
import useAuthContext from '../../modules/auth/logic/provider'
import {SearchBox} from './search_box'
interface IParamData {
  [key: string]: ICategory
}

export interface IElementMenuType {
  title?: string
  link?: string
  variant?: string
  type?: string
  sub?: IElementMenuType[]
}
type Params = { [K in keyof IParamData]: string }
export const MenuStoreNav: React.FC = () => {
  const {userInfo}: IAuthUser = useAuthContext()
  const [currentSubMenu, setCurrentSubMenu] = useState('')
  const [isShow, setIsShow] = useState(false)
  const {categories, getCategoriesAction }: ICategoryContext  = useCategoriesContext()
  const param = useParams<Params>()
  const history = useHistory()
  const {getCartItemVariants, cartItemVariants} : ICartContextProps = useCartContext()

  useEffect(() => {
    getCategoriesAction(param?.categoryID)
     // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (userInfo.userID === '') return
    getCartItemVariants(userInfo.userID)
     // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userInfo])

const Buttons: IElementMenuType[] = [
  {
    title: 'Your Store',
    link: '/',
    variant: 'box_nav',
  },
  {
    title: 'New & NoteWorthy',
    variant: 'box_new_noteWorthy',
    type: '1',
    sub: [
      {
        title: 'Top Sellers',
        link: '/topsellers',
      },
      {
        title: 'New & Trending',
        link: '/trending',
      },
      {
        title: 'Current Specials',
        link: '/specials',
      },
      {
        title: 'Popular Upcoming',
        link: '/upcoming',
      },
    ],
  },
  {
    title: 'Categories',
    variant: 'box_new_noteWorthy',
    type: '2',
    sub: [{...categories}]
  },
  {
    title: 'Project',
    variant: 'box_new_noteWorthy',
    link: '/projects',
    type: '3'
  }
]

  const handleOnMouseLeave = () => {
    setCurrentSubMenu('');
    setIsShow(false);
  }

  const handleOnMouseEnter = (type: string) => {
    setCurrentSubMenu(type)
    setIsShow(true);
  }

  const numberItemInCart = useMemo(() => Object.values(cartItemVariants).length , [cartItemVariants])

  return (
    <>
      <Flex
        sx={{
          position: 'relative',
          justifyContent: 'center',
          maxWidth: '990px',
          margin: '0 auto',
          flexWrap: 'wrap',
          marginBottom: '30px',
        }}
      >
        <Box variant='box_menu_cart_all' sx={{ width: ['none', 'none', '96%', '95%'], marginTop: '5px' }}>
          {numberItemInCart > 0 &&
            <Button variant='button_menu_cart' onClick={() => history.push('/cart')}>
              CART {numberItemInCart}
            </Button>
          }
        </Box>
        <Box variant='box_menu_no_responsive'>
          <Box variant='box_menu_no_responsive_2'>
            {Buttons.map((item, index) => {
              return (
                  <CommonMenu
                    key={index}
                    menu={item}
                    currentShowSub={currentSubMenu}
                    onMouseEnter={handleOnMouseEnter}
                    onMouseLeave={handleOnMouseLeave}
                    handleClick={setCurrentSubMenu}
                    isShow={isShow}
                    setIsShow={setIsShow}
                  />
              )
            })}
          </Box>
          <SearchBox/>
        </Box>
      </Flex>
    </>
  )
}