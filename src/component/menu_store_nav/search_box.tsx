import { Box, Button } from 'rebass'
import { Input } from '@rebass/forms'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faTimes } from '@fortawesome/free-solid-svg-icons'
import { useEffect, useRef, useState } from 'react'
import { IItemsContextProps } from '../../modules/items/type'
import useItemsContext from '../../modules/items/logic/providers'
import { hooks } from 'react-cores'
import { SearchBoxResult } from './search_result'

export const SearchBox: React.FC = () => {
  const [searchName, setSearchName] = useState<string>(null!)
  const [showResult, setShowResult] = useState<boolean>(true)
  const {getSearchItem, setSearchItemResult, searchItemResult}: IItemsContextProps = useItemsContext()
  const InputRef = useRef<HTMLInputElement>(null)

  useEffect(() => {
    const handleClickDrop = (e: Event) => {
      if (InputRef.current && InputRef.current.contains(e.target as Node)) {
        setShowResult(true)
      } else {
        setShowResult(false)
      }
    }

    window.addEventListener('click', handleClickDrop)

    return () => window.removeEventListener('click', handleClickDrop)
  }, [])

  hooks.useDebounce(() => {
    if (searchName && searchName.length > 0) {
      getSearchItem({name: searchName})

      return false
    }
    setSearchItemResult({})
  }, [searchName], 500)

  return (
    <Box variant='box_menu_search'>
      <Input
        type='text'
        placeholder='search'
        value= {searchName || ''}
        onChange={(e) => setSearchName(e.target.value)}
        ref = {InputRef}
        sx={{
          width: ['100%', '100%', '84%', '84%'],
          outline: 'none',
          height: '25px',
          border: 'none',
          color: '#fff',
          background: 'transparent',
          fontSize: ['17px', '17px', '15px', '15px'],
      }}/>
      {searchName !== null && searchName?.length > 0 && (
        <Button
          sx={{
            cursor: 'pointer',
            background: 'none',
            color: '#fff',
            ':hover': {
              color: '#444',
            },
          }}
          onClick={() => setSearchName('')}
        >
          <FontAwesomeIcon icon={faTimes} />
        </Button>
      )}
      <Box
        sx={{
          width: '32px',
          height: '32px',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: '3px',
          background: 'rgb(66, 221, 245, 0.2)',
        }}>
          <FontAwesomeIcon
            icon={faSearch}
            style={{ color: '#93908d' }}
            className='dropdown-icon-md'
          />
      </Box>
      {showResult && Object.values(searchItemResult).length > 0  && <SearchBoxResult setSearchName={setSearchName}/>}
    </Box>
  )
}