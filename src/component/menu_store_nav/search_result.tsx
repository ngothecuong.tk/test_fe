import { Box, Flex, Image, Text } from 'rebass';
import { IItemsContextProps } from '../../modules/items/type';
import useItemsContext from '../../modules/items/logic/providers';
import { useHistory } from 'react-router-dom';

interface IInitialProps {
  setSearchName: (value: string) => void
}

export const SearchBoxResult: React.FC<IInitialProps> = ({setSearchName}) => {
    const {searchItemResult}: IItemsContextProps = useItemsContext()
    const history = useHistory()

    return (
      <Box
        sx={{
          width: ['100%', '100%', '430px', '430px'],
          right: 0,
          top: '33px',
          display: 'block',
          position: 'absolute',
          zIndex: 1000,
          p: '8px 5px',
          background: 'linear-gradient(90deg, rgba(33, 162, 255, 0.1) 1.89%, rgba(50, 50, 51, 0) 50%), linear-gradient(180deg, #575860 11.6%, #3A4852 54.73%, #2C2D34 100%)',
          boxShadow: '0 0 12px #000000',
      }}>
        {searchItemResult &&
          Object.values(searchItemResult).map((item, index) => (
            <Box
              key={index}
              onClick={() => {
                setSearchName(null!)
                history.push(`/items/${item.itemID}`)
              }}
              sx={{
                display: 'block',
                textDecoration: 'none',
                color: '#D8D8D8',
                fontSize: '15px',
                cursor: 'pointer',
                ':hover': {
                  color: '#fff',
                  background: '#212d3d',
                }
              }}>
                <Flex
                  sx={{
                    padding: ['5px 6px'],
                    alignItems: 'center',
                    borderTop: '1px solid #13242e'
                }}>
                  <Image
                    src={item.thumbnail}
                    sx={{
                      marginRight: ['10px'],
                      width: '120px',
                      height: '55px',
                      objectFit: 'cover',
                  }}/>
                  <Box sx={{textAlign: 'left'}}>
                    <Text>{item.name}</Text>
                    <Flex sx={{fontSize: '13px', mt: '5px'}}>
                      <Text>{item.price}</Text>
                      <Text>{item.currency}</Text>
                    </Flex>
                  </Box>
                </Flex>
            </Box>
          ))}
      </Box>
    );
};
