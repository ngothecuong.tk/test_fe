import { Box, Text } from 'rebass'
import { IElementMenuType } from '../component/menu_store_nav/index'
import { hooks } from 'react-cores'
import { Link, useHistory } from 'react-router-dom'

interface IProps {
  menu?: IElementMenuType
  currentShowSub: string
  onMouseEnter: (type: string) => void
  onMouseLeave: VoidFunction
  handleClick: (type: string) => void
  setIsShow: (isShow: boolean) => void
  isShow: boolean
}
export const CommonMenu: React.FC<IProps> = ({
  menu,
  onMouseEnter,
  onMouseLeave,
  currentShowSub,
  handleClick,
  setIsShow,
  isShow,
}) => {
  const { isDesktop }: hooks.IResponsive = hooks.useResponsive()
  const history = useHistory()
  const MenuShowHover = () => {
    return (
      <>
        <Box
          onMouseEnter={() => {
            if (!isDesktop) {
              return
            }
            onMouseEnter(menu?.type as string)
          }}
          onMouseLeave={() => {
            onMouseLeave()
          }}
          onClick={() => {
            if (isDesktop) {
              return
            }

            setIsShow(!isShow)
            handleClick(menu?.type as string)
          }}
          variant='box_all_title_submenu'
        >
          <Box
            sx={{
              ':hover': {
                background:
                  'linear-gradient(90deg, rgba(33, 162, 255, 0.25) 0%, rgba(33, 162, 255, 0.15) 50%, rgba(50, 50, 51, 0) 100%)',
              },
            }}
          >
            <Box
              variant={menu?.variant}
              href={menu?.link}
              onClick={() => {
                menu?.link ? history.push(`${menu?.link}`) : history.replace('/')
              }}
            >
              {menu?.title}
            </Box>
          </Box>
          {currentShowSub === menu?.type && menu?.sub?.length && isShow && (
            <Box variant='box_hover_submenu'>
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  paddingLeft: '8px',
                }}
              >
                {menu.sub?.map((name, num) => {
                  return (
                    <Box key={num}>
                      {name.title ? (
                        <Box
                          sx={{
                            display: 'flex',
                            flexDirection: 'column',
                          }}
                        >
                          <Box variant='link_hover_submenu' onClick={() => history.push(`${name.link}`)}>{name.title}</Box>
                        </Box>
                      ) : (
                        <Box
                          sx={{
                            display: 'flex',
                            flexDirection: 'column',
                          }}
                        >
                          {Object.values(name).map((item, idx) => {
                            return (
                              <Text
                                key={idx}
                                variant='text_nav_menu'
                              >
                                <Link
                                  style={{
                                    cursor: 'pointer',
                                    position: 'relative',
                                    color: '#d9d9d9',
                                    fontFamily: 'system-ui',
                                    fontSize: '14px',
                                    borderBottom: '2px solid transparent',
                                    textDecoration: 'none',
                                  }}
                                  to={`/categories/${item.categoryID}`}
                                >
                                  {item?.name}
                                </Link>
                              </Text>
                            )
                          })}
                        </Box>
                      )}
                    </Box>
                  )
                })}
              </Box>
            </Box>
          )}
        </Box>
      </>
    )
  }

  return (
    <>
      <MenuShowHover />
    </>
  )
}
