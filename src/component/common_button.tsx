import React from 'react'
import { Link } from 'rebass'
interface IProps {
  title: string,
  link: string,
  variant: string,
}
export const CommonButton: React.FC<IProps> = ({ title, link, variant}) => {

  return (
    <Link variant={variant} href={link}>
      {title}
    </Link>
  )
}