import { Footer } from '../footer'
import { HeaderUI } from '../header'
import { MenuStoreNav } from '../menu_store_nav'

export const Layout: React.FC = ({ children }) => {
  return (
    <>
      <HeaderUI/>
      <MenuStoreNav/>
      <main style={{ overflow: 'hidden' }} >
        {children}
      </main>
      <Footer/>
    </>
  )
}