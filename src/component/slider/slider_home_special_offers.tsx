import {Box, Image, Text, Link } from 'rebass'
import { IProjects } from '../../modules/projects/type';

interface IInitialProps {
  project: IProjects
}

export const SliderHomeSpecialOffer: React.FC<IInitialProps> = ({project}) => {

  return (
    <Box sx={{
      position: 'relative',
      height: '390px',
      boxShadow: '0 0 5px #000'
    }}>
      <Link sx= {{textDecoration: 'none'}} href={`/project/${project.projectID}`}>
      <Box>
        <Image src={project.banner}
          sx={{
            height: '350px',
            objectFit: 'cover'
          }}
        />
      </Box>
      <Box sx={{
        p: '8px 16px 12px',
        color: '#9099a1',
        fontSize: '12px',
        backgroundColor: '#1A6487',
        position: 'absolute',
        bottom: '0',
        width: '100%',
        textAlign: 'left'
      }}>
        <Text sx={{
          fontFamily: '"Motiva Sans", Sans-serif',
          color: '#fff',
          m: '0 0 5px',
          letterSpacing: '0.03rem',
          paddingTop: '2px',
          textTransform: 'uppercase',
          fontSize: '14px'
        }}>
          {project.name}
        </Text>
        <Text sx={{
          color: '#acdbf5',
          letterSpacing: '0.03rem',
          textTransform: 'capitalize',
          fontFamily: '"Motiva Sans", Sans-serif',
          overflow: 'hidden',
          display: '-webkit-box',
          WebkitLineClamp: '5',
          WebkitBoxOrient: 'vertical'
        }}>
          {project.description}
        </Text>
      </Box>
      </Link>
    </Box>
  );
}