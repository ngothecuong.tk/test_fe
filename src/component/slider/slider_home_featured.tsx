import {Box, Text, Link } from 'rebass'
import { IProjects } from '../../modules/projects/type';
interface IInitialProps {
  project: IProjects
}

export const SliderHomeFeatured: React.FC<IInitialProps> = ({project}) => {
  return (
    <Link sx= {{textDecoration: 'none'}} href={`/project/${project.projectID}`}>
    <Box sx={{
        display: ['inline-block', 'inline-block', 'inline-block', 'flex'],
        width: '100%',
        overflow: 'hidden',
      }}
    >
      <Box variant='box__slider_imgBanner'
        sx={{
          backgroundImage: `url(${project.banner})`,
          backgroundPosition: 'center center',
          backgroundSize: 'cover'
        }}
      />
      <Box>
        <Box variant='box__slider_infoName'>
          <Text variant='text__slider_infoName'>
            {project.name}
          </Text>
        </Box>
        <Box variant='box__slider_infoScreenshots'>
          {project?.photos?.map((photo, i) => {
            return (
              <Box variant='box__slider_infoScreenshots_img' key={i}>
                <Box variant='box__slider_infoScreenshots_img'
                  sx={{
                    backgroundImage: `url(${photo})`,
                    backgroundPosition: 'center center',
                    backgroundSize: 'cover',
                    padding: '0'
                  }}/>
                </Box>
                )
            })}
        </Box>
        <Box variant='box__slider_infoReason'>
          <Box variant='box__infoReason_description'>
            {project.intro}
          </Box>
        </Box>
      </Box>
    </Box>
    </Link>
  );
}