import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleRight, faAngleLeft } from '@fortawesome/free-solid-svg-icons'
import { Button } from 'rebass'
import { CustomArrowProps } from 'react-slick'

interface IButtonNext extends CustomArrowProps {
  right?: string
}
interface IButtonPrev extends CustomArrowProps {
  left?: string
}

export function NextArrowButton({ className, style, onClick, right }: IButtonNext) {
  return (
    <Button
      className={className}
      variant='button__slider_right'
      style={{ ...style }}
      onClick={onClick}
      sx={{right}}
    >
      <FontAwesomeIcon
        icon={faAngleRight}
        style={{
          width: '25px',
          height: '50px',
        }}
      />
    </Button>
  )
}
export function PrevArrowButton({ className, style, onClick, left }: IButtonPrev) {
  return (
    <Button
      className={className}
      variant='button__slider_left'
      style={{ ...style }}
      onClick={onClick}
      sx={{left}}
    >
      <FontAwesomeIcon
        icon={faAngleLeft}
        style={{
          width: '25px',
          height: '50px',
        }}
      />
    </Button>
  )
}