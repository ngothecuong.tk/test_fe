import Slider, { ResponsiveObject } from 'react-slick'
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { NextArrowButton, PrevArrowButton } from './button_slider';
import './style.css'
interface IInitialProps {
  slideShow?: number,
  slideScroll?: number
  showFade?: boolean
  showResponsive?: ResponsiveObject[]
  showNextArrow: string,
  showPrevArrow: string,
}

export const Sliders: React.FC<IInitialProps>  = ({children, slideShow, slideScroll,
  showFade, showResponsive, showNextArrow, showPrevArrow}) => {

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: slideShow,
    slidesToScroll: slideScroll,
    fade: showFade,
    nextArrow: <NextArrowButton right={showNextArrow}/>,
    prevArrow: <PrevArrowButton left={showPrevArrow}/>,
    customPaging: () => (
      <div
        style={{
          width: '20px',
          height: '9px',
          background: 'hsla(202,60%,100%,0.2)',
          cursor: 'pointer',
          borderRadius: '2px',
          transitionDuration: 'background 0.3s'
        }}
      />
    ),
    responsive: showResponsive
  }

  return (
    <Slider
      {...settings}
    >
      {children}
    </Slider>
  );
}
