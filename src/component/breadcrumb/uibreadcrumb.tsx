import { faAngleRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { Link } from 'react-router-dom'
import { Box, Text } from 'rebass'

interface IBreadCrumb {
  icon?: () => void
  name: string
  link?: string
}
interface IInitialProps {
  breadCrumbData: IBreadCrumb[]
}
export const BreadcrumbUi: React.FC<IInitialProps> = ({ breadCrumbData }) => {
  return (
    <>
      {breadCrumbData.map((item: IBreadCrumb, index: number) => {
        const isLast = breadCrumbData.length - 1 === index
        if (!isLast) {
          return (
            <Box
              as='li'
              sx={{
                display: 'flex',
                alignItems: 'center',
                color: index === 0 ? '#777777' : 'inherit',
                fontWeight: index === 0 ? '600' : '300',
                ':hover': {
                  color: '#fff',
                },
              }}
              key={index}
            >
              {item.icon && (
                <Box as='span' m='2px'>
                  <>{item.icon()}</>
                </Box>
              )}
              <Link
                style={{
                  color: 'inherit',
                  textDecoration: 'none',
                }}
                to={`${item.link}`}
              >{`${item.name}`}</Link>
              <FontAwesomeIcon
                icon={faAngleRight}
                style={{ padding: '0px 7px', fontSize: '15px', color: '#888' }}
              />
            </Box>
          )
        }

        return (
          <Box as='li' key={index}>
            <Text as='span'>{`${item.name}`}</Text>
          </Box>
        )
      })}
    </>
  )
}
