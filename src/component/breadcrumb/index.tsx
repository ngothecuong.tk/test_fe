import { Box } from 'rebass'
import { BreadcrumbUi } from './uibreadcrumb'
interface IBreadCrumb {
  icon?: () => void
  name: string
  link?: string
}
interface IInitialProps {
  breadCrumbData: IBreadCrumb[]
}

export const Breadcrumbs: React.FC<IInitialProps> = ({ breadCrumbData }) => {
  return (
    <Box sx={{ py: '16px' }}>
      <Box variant='box__Container'>
        <ol
          style={{
            listStyle: 'none',
            display: 'flex',
            alignItems: 'center',
            fontSize: '16px',
            fontFamily: 'sans-serif',
            color: '#888',
            padding: 0,
            margin: 0,
          }}
        >
          <BreadcrumbUi breadCrumbData={breadCrumbData} />
        </ol>
      </Box>
    </Box>
  )
}
