import { HomeProvider } from '../modules/home/logic/providers'
import { ProjectsProvider } from '../modules/projects/logic/providers'
import {AuthProvider} from '../modules/auth/logic/provider'
import { CategoriesProvider } from '../modules/categories/logic/provider'
import { ItemsProvider } from '../modules/items/logic/providers'
import { ItemVariantsProvider } from '../modules/item_variants/logic/provider'
import { CartProvider } from '../modules/cart/logic/providers'

export const providers = [
  CategoriesProvider,
  HomeProvider,
  ProjectsProvider,
  AuthProvider,
  ItemsProvider,
  CategoriesProvider,
  ItemVariantsProvider,
  CartProvider
]
