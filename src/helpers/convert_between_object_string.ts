export function convertStringToObjectUrl(str: string) {
  const result: { [key: string]: string[] | string | number } = {}
  if (str) {
    str
      .split('?')[1]
      .split('&')
      .forEach((obj) => {
        const key = obj.split('=')[0].split('[')[0]
        const value = Number(obj.split('=')[1]) || obj.split('=')[1]
        if (obj.split('=')[0].substr(-1) === ']') {
          result[key] =
            typeof result[key] === 'object'
              ? ([...(result[key] as string[]), value] as string[])
              : ([value] as string[])
        } else {
          result[key] = value
        }
      })
  }

  return result
}