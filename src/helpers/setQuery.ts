export const setQuery = (querys: { [key: string]: string }) => {
  if (!Object.keys(querys)?.length) {
    return null
  }

  let queryValue = ''

  for (const query in querys) {
    if (!querys[query] || querys[query] === 'undefined') {
      continue
    }

    queryValue = queryValue.concat('&', `${query}=${querys[query]}`)
  }

  return queryValue
}
