// tslint:disable-next-line:no-any
export function convertArrayObjectToObject<T extends { [key: string]: any }>(array: T[], key: string) {
  if (!array?.length) {
    return {}
  }

  return array.reduce((obj, item) => {
    return {
      ...obj,
      [item[key]]: item,
    }
  }, {})
}
