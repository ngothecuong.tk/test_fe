export function calculateDiscountPercentage(price: number, discountPrice: number): string {
  return `${Math.round(((discountPrice - price) * 100) / price)}%`
}