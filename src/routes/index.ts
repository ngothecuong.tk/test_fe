import { hoc } from 'react-cores'
import { Home } from '../modules/home/index.page'
import { SignIn } from '../modules/auth/ui/signin'
import { ProFile } from '../modules/auth/profile'
import { ItemVariants } from '../modules/item_variants/index.page'
import { Gamedetails } from '../modules/gamedetail/index'
import { Category } from '../modules/categories/index.page'
import { Project } from '../modules/projects/index.page'
import { NewNoteworthy } from '../modules/new_noteworthy'
import { Cart } from '../modules/cart/index.page'

export const routes: hoc.IRoute[] = [
  {
    path: '/',
    component: Home,
    isExact: true,
  },
  {
    path: '/signin',
    component: SignIn,
    isExact: true,
  },
  {
    path: '/profile',
    component: ProFile,
    isExact: true,
  },
  {
    path: '/categories/:categoryID',
    component: Category,
    isExact: true,
    },
  {
    path: '/items/:itemID',
    component: ItemVariants,
    isExact: true,
  },
  {
    path: '/project/:projectID',
    component: Gamedetails,
    isExact: true,
  },
  {
    path: '/projects',
    component: Project,
    isExact: true
  },
  {
    path: '/cart',
    component: Cart,
    isExact: true
  },
  {
    path: '/:filter',
    component: NewNoteworthy,
    isExact: true
  },
]
