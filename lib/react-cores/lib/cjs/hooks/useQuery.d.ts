export declare const useQuery: () => {
    [k: string]: string;
};
