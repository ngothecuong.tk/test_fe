export declare const useDebounce: (callback: Function, deps: any[], delay: number) => void;
