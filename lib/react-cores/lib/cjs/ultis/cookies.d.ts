export interface ISetCookieOptional {
    days?: number;
    isPrivate?: boolean;
}
export declare const setCRSFCookies: (value: string, { days }: {
    days?: number | undefined;
}) => void;
export declare const cookies: {
    write: (name: string, value: string, { days, isPrivate }: ISetCookieOptional) => void;
    read: (name: string) => string | null | undefined;
    erase: (name: string) => void;
};
