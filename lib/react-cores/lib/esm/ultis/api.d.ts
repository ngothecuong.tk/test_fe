import { Method } from 'axios';
export interface ApiDataType {
    path: string;
    data?: object;
    method: Method;
    params?: object;
    headers?: object;
    errorHandler?: (error: any) => void;
}
export declare const api: ({ path, method, data, params, errorHandler, headers }: ApiDataType) => Promise<void | import("axios").AxiosResponse<any>>;
