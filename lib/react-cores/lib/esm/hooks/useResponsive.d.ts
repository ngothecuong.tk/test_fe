export interface IResponsive {
    isMobile: boolean;
    isTablet: boolean;
    isDesktop: boolean;
}
export declare const useResponsive: () => IResponsive;
