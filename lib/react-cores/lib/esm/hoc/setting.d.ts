import { SnackbarOrigin } from 'notistack';
import React from 'react';
import { ApiDataType } from '../ultis/api';
interface IAuthConfig {
    thirdPartyRedirect?: string;
    redirectPath?: string;
    userInfoApi?: ApiDataType;
    context: React.Context<any>;
}
export interface IBaseConfig {
    base_url: string;
    shouldUseCookie: boolean;
    auth: IAuthConfig;
    baseRoute: string;
    notiPosition?: SnackbarOrigin;
    publicPaths?: string[];
}
export interface ISettingHOC {
    routes: IRoute[];
    providers: React.FC[];
    ErrorPage?: React.FC;
    Layout?: React.FC;
    baseConfig: IBaseConfig;
    onUserInfo?: (AxiosResponse: any, context: any) => void;
}
export interface IRoute {
    path: string;
    component: React.FC;
    isExact: boolean;
}
export declare const SettingHOC: React.FC<ISettingHOC>;
export {};
