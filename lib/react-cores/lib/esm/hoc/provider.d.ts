/// <reference types="react" />
export interface IProviderHOCProps {
    providers: React.FC[];
}
export declare const ProviderHOC: React.FC<IProviderHOCProps>;
